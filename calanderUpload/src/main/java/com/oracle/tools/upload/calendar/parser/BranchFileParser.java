package com.oracle.tools.upload.calendar.parser;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.dataholders.BranchCalDataHolder;
import com.oracle.tools.upload.calendar.dataholders.BranchCalKeyHolder;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

import lombok.Getter;
import lombok.Setter;

public class BranchFileParser {
	@Setter
	private String invalidFileData;
	private volatile String errorData;
	@Getter
	Map<BranchCalKeyHolder, List<BranchCalDataHolder>> parsedData;
	private List<String> invalidData;
	private String partUploadFailed = "PART_WRONG_FORMAT";

	public boolean parseFile(String fileNameAppender) {
		parsedData = new HashMap<BranchCalKeyHolder, List<BranchCalDataHolder>>();
		String fileName = ApplicationConstants.getValue("FILE_NAME");
		String directory = ApplicationConstants.getValue("PREPROCESS_DIR");
		String filePath = directory + File.separator + fileNameAppender + fileName;
		File f = new File(filePath);
		Path file = f.toPath();
		Charset charset = Charset.defaultCharset();
		if (f.exists()) {
			try {
				Files.lines(file, charset).forEach(line -> {
					if (line != null && !line.isEmpty()) {
						try {
							BranchCalKeyHolder keyDataHolder;
							int year = 0;
							BranchCalDataHolder lineData = new BranchCalDataHolder();
							String branchCode = line.substring(0, 3).trim();
							year = Integer.parseInt(line.substring(3, 7).trim());
							keyDataHolder = new BranchCalKeyHolder(branchCode, year);
							lineData.setMonth(Integer.parseInt(line.substring(7, 9).trim()));
							lineData.setDate(Integer.parseInt(line.substring(9, 11).trim()));
							lineData.setHolidayType(line.charAt(11));
							if (parsedData.get(keyDataHolder) == null) {
								parsedData.put(keyDataHolder, new ArrayList<BranchCalDataHolder>());
							}
							parsedData.get(keyDataHolder).add(lineData);
						} catch (Exception e) {
							errorData = line;
							if(invalidData == null)
								invalidData = new ArrayList<String>();
							invalidData.add(line);
							//throw e; /*Commented this line to process valid lines in Upload file */
						}

					}

				});
			} catch (Exception e) {
				ApplicationLogger.fatal(ERROR_LOGGER, "Failed while parsing data: " + errorData, e);
				ErrorHandler.logError(invalidFileData);
				//return false; /*Commented this line to process valid lines in Upload file */
			}
		}
		if(invalidData != null) {
			ErrorHandler.logError(this.partUploadFailed);
		}
		return true;
	}
}
