package com.oracle.tools.upload.calendar.FileSplitter;
import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import lombok.Setter;
@Setter
public class CurrencyCalendarFileSplitter implements IFileSplitter {
    private String preProcessDirRef;
    private List<String> currFormatsList;
    private boolean errorFile = false;
    private String errorUploadFailed = "CURR_WRONG_FORMAT";
    private String partUploadFailed = "PART_WRONG_FORMAT";
    @Override
    public boolean splitFile(Path file) {
        boolean result = true;
        String fileName = file.getFileName().toString();
        ApplicationConstants.setValue("FILE_NAME", fileName);
        if (!streamAndProcess(file, fileName, "VALIDATE"))
            result = false;
        if (result)
            result = currFormatsList.stream().allMatch(identifier -> streamAndProcess(file, fileName, identifier));
        return result;
    }
    private boolean streamAndProcess(Path file, String fileName, String identifier) {
        boolean result = true;
        try (Stream<String> stream = Files.lines(file)) {
            switch (identifier) {
            case "VALIDATE":
                String validIdentifiers = currFormatsList.stream().collect(Collectors.joining("|"));
//              List<String> invalidData = stream.filter(line -> line.matches("^(?!HS|HF).*$")).collect(Collectors.toList());
                List<String> invalidData = stream.filter(line -> line.matches("^(?!"+validIdentifiers+").*$")).collect(Collectors.toList());
                if (invalidData.size() > 0) {
                    ApplicationLogger.error(ERROR_LOGGER, "File contains wrong Format" + invalidData);
                    errorFile = true;
                    result = true;
                    ErrorHandler.logError(this.errorUploadFailed);
                }
                break;
            default:
                createFile(identifier, fileName, stream);
                break;
            }
        } catch (IOException e) {
            ApplicationLogger.fatal(ERROR_LOGGER, "File Split Failed", e);
            result = false;
        }
        return result;
    }
    private void createFile(String identifier, String fileName, Stream<String> stream) throws IOException {
        List<String> fileData = stream.filter(line -> line.startsWith(identifier)).collect(Collectors.toList());
        if (identifier.equals("HS"))
            fileData = fileData.parallelStream().map(p -> {
                String formatedValue = "##" + p.charAt(2) + p.substring(6, 14) + p.charAt(14);
                return formatedValue;
            }).collect(Collectors.toList());
        else
            fileData = fileData.parallelStream().map(p -> {
                String formatedValue = p.substring(3, 5) + p.charAt(2) + p.substring(75, 83) + p.charAt(83);
                return formatedValue;
            }).collect(Collectors.toList());
        if (fileData != null && fileData.size() > 0) {
            FileWriter writer = null;
            String newFilePath = ApplicationConstants.getValue(preProcessDirRef) + File.separator + "MERGED" + fileName;
            writer = new FileWriter(newFilePath, true);
            for (String str : fileData) {
                writer.write(str + System.lineSeparator());
            }
            if(errorFile)
            	ErrorHandler.logError(this.partUploadFailed);
            writer.close();
        }
    }
}
