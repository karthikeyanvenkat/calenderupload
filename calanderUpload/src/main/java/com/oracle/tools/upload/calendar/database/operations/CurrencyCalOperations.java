package com.oracle.tools.upload.calendar.database.operations;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.oracle.tools.upload.calendar.constants.UploadUsers;
import com.oracle.tools.upload.calendar.context.ApplicationContextProvider;
import com.oracle.tools.upload.calendar.dataholders.CurrencyCalKeyHolder;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

public class CurrencyCalOperations {
	UploadUsers users;
	Connection connection;
	Connection connectionEntity;
	Map<String, List<String>> countryCodeCurrencyMap;
	Set<String> currencySet;

	public CurrencyCalOperations(Connection connection, Connection connectionEntity) {
		this.connection = connection;
		this.connectionEntity = connectionEntity;
	}

	public boolean createYearData(Set<CurrencyCalKeyHolder> keySet) {
		
		if(!fetchCountryCodeCurrencyMap())
			return false;
		users = (UploadUsers) ApplicationContextProvider.getContext().getBean("users");
		currencySet = new HashSet<String>();
		
		try {
			keySet.forEach((keyData) -> {
				//String currency = countryCodeCurrencyMap.get(keyData.getCountryCode());
				List<String> currencyList = countryCodeCurrencyMap.get(keyData.getCountryCode());
				if(currencyList != null)
					currencyList.forEach((currency) -> {
						if (!currencySet.contains(currency + keyData.getYear())) {
							currencySet.add(currency + keyData.getYear());
							try (PreparedStatement statement = connection
									.prepareStatement("SELECT COUNT(1) FROM STTMS_CCY_HOL_MASTER WHERE CCY=? and YEAR=?")) {
								statement.setString(1, currency);
								statement.setInt(2, keyData.getYear());
								try (ResultSet rs = statement.executeQuery()) {
									while (rs.next()) {
										int resultCount = rs.getInt(1);
										if (resultCount == 0)
											if (!createEntry(keyData.getYear(), currency))
												throw new Exception(
														"Failed in creating monthly data. createEntry returned false for Year="
																+ keyData.getYear() + " Currency: " + currency);
									}
								}
							} catch (Exception exception) {
								ErrorHandler.logError("QUERY-ERROR-001",
										"Failed for year : " + keyData.getYear() + " Currency: " + currency);
								ApplicationLogger.fatal(ERROR_LOGGER,
										"Failed for " + keyData.getYear() + " Currency: " + currency, exception);
								throw new RuntimeException(exception);
							}
						}
					});
				else if(keyData.getCountryCode().equals("##")) {
					final String eurCurrency = "EUR";
					if (!currencySet.contains(eurCurrency + keyData.getYear())) {
						currencySet.add(eurCurrency + keyData.getYear());
						try (PreparedStatement statement = connection
								.prepareStatement("SELECT COUNT(1) FROM STTMS_CCY_HOL_MASTER WHERE CCY=? and YEAR=?")) {
							statement.setString(1, eurCurrency);
							statement.setInt(2, keyData.getYear());
							try (ResultSet rs = statement.executeQuery()) {
								while (rs.next()) {
									int resultCount = rs.getInt(1);
									if (resultCount == 0)
										if (!createEntry(keyData.getYear(), eurCurrency))
											throw new Exception(
													"Failed in creating monthly data. createEntry returned false for Year="
															+ keyData.getYear() + " Currency: " + eurCurrency);
								}
							}
						} catch (Exception exception) {
							ErrorHandler.logError("QUERY-ERROR-001",
									"Failed for year : " + keyData.getYear() + " Currency: " + eurCurrency);
							ApplicationLogger.fatal(ERROR_LOGGER,
									"Failed for " + keyData.getYear() + " Currency: " + eurCurrency, exception);
							throw new RuntimeException(exception);
						}
					}
				}
			});
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private boolean createEntry(Integer year, String currency) {
		String fullWorkingString = "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW";
		String query = "INSERT INTO STTM_CCY_HOL_MASTER(CCY, YEAR, MAKER_ID, ONCE_AUTH, AUTH_STAT, MOD_NO, RECORD_STAT, MAKER_DT_STAMP, WEEKLY_HOLIDAYS, CHECKER_DT_STAMP, CHECKER_ID)"
				+ "VALUES(?,?,?,'Y', 'A',1, 'O',CURRENT_DATE, null ,CURRENT_DATE , ?)";
		int month = 0;
		String monthStringTemp = null;
		try (PreparedStatement pmtmNetworkHolidayMaster = connection.prepareStatement(query)) {
			pmtmNetworkHolidayMaster.setString(1, currency);
			pmtmNetworkHolidayMaster.setInt(2, year);
			pmtmNetworkHolidayMaster.setString(3, users.getUser("MAKER_ID"));
			pmtmNetworkHolidayMaster.setString(4, users.getUser("CHECKER_ID"));
			boolean insertedSuccessfully = pmtmNetworkHolidayMaster.execute();
			for (month = 1; month <= 12; month++) {
				// Get the number of days in that month
				YearMonth yearMonthObject = YearMonth.of(year, month);
				int daysInMonth = yearMonthObject.lengthOfMonth();
				StringBuffer monthString = new StringBuffer(fullWorkingString.substring(0, daysInMonth));
				List<LocalDate> weekendList = getWeekends(year, month);
				weekendList.forEach(satSun -> {
					int dayOfWeek = satSun.getDayOfMonth();
					monthString.setCharAt(dayOfWeek - 1, 'H');
				});
				monthStringTemp = monthString.toString();
				try (PreparedStatement pmtmNetworkHolidayInsert = connection.prepareStatement(
						"INSERT INTO STTM_CCY_HOLIDAY(CCY,  YEAR, MONTH, HOLIDAY_LIST)" + "VALUES(?,?,?,?)")) {

					pmtmNetworkHolidayInsert.setString(1, currency);
					pmtmNetworkHolidayInsert.setInt(2, year);
					pmtmNetworkHolidayInsert.setInt(3, month);
					pmtmNetworkHolidayInsert.setString(4, monthString.toString());
					insertedSuccessfully = pmtmNetworkHolidayInsert.execute();
				}
			}

		} catch (Exception e) {
			ErrorHandler.logError("QUERY-ERROR-002", "INSERT INTO FSTM_CLS_HOLIDAY Failed");
			ApplicationLogger.error(ERROR_LOGGER, "INSERT INTO FSTM_CLS_HOLIDAY(YEAR, MONTH, HOLIDAY_LIST)");
			ApplicationLogger.error(ERROR_LOGGER, "year: " + year);
			ApplicationLogger.error(ERROR_LOGGER, "month: " + month);
			ApplicationLogger.error(ERROR_LOGGER, "monthString: " + monthStringTemp);
			ApplicationLogger.error(ERROR_LOGGER, "currency: " + currency);
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in STTM_LCL_HOLIDAY INSERT", e);
			return false;
		}
		return true;
	}

	private List<LocalDate> getWeekends(int year, int month) {
		LocalDate firstDateOfTheMonth = YearMonth.of(year, month).atDay(1);
		List<LocalDate> list = new ArrayList<>();
		for (LocalDate date = firstDateOfTheMonth; !date
				.isAfter(firstDateOfTheMonth.with(TemporalAdjusters.lastDayOfMonth())); date = date.plusDays(1))
			if (date.getDayOfWeek() == DayOfWeek.SATURDAY || date.getDayOfWeek() == DayOfWeek.SUNDAY)
				list.add(date);
		return list;
	}

	private boolean fetchCountryCodeCurrencyMap() {
		countryCodeCurrencyMap = new HashMap<String, List<String>>();
		try (Statement statement = connectionEntity.createStatement()) {
			try (ResultSet rs = statement.executeQuery("SELECT country, ccy_code FROM CYTM_CCY_DEFN_MASTER")) {
				while (rs.next()) {
					if (!countryCodeCurrencyMap.containsKey(rs.getString(1))) {
						countryCodeCurrencyMap.put(rs.getString(1), new ArrayList<String>());
			        }
			        countryCodeCurrencyMap.get(rs.getString(1)).add(rs.getString(2));
				}
			}
		} catch (Exception exception) {
			ErrorHandler.logError("QUERY-ERROR-005",
					"Failed for creating CountryCodeCurrencyMap table CYTM_CCY_DEFN_MASTER: ");
			ApplicationLogger.fatal(ERROR_LOGGER,
					"Failed for creating CountryCodeCurrencyMap table CYTM_CCY_DEFN_MASTER: ", exception);
			return false;
		}
		return true;
	}

	public Map<CurrencyCalKeyHolder, String> getMonthlyData(CurrencyCalKeyHolder keyData) {
		Map<CurrencyCalKeyHolder, String> yearDataDB = new HashMap<CurrencyCalKeyHolder, String>();
		String query = "SELECT month, holiday_list FROM STTM_CCY_HOLIDAY WHERE ccy=? AND year=?", eurCurrency;
		List<String> currencyList = countryCodeCurrencyMap.get(keyData.getCountryCode());
		if(currencyList != null)
			currencyList.forEach((currency) -> {
				try (PreparedStatement sttmLclHolidaySelect = connection.prepareStatement(query)) {
					
						sttmLclHolidaySelect.setString(1, currency);
						sttmLclHolidaySelect.setInt(2, keyData.getYear());
						try (ResultSet rs = sttmLclHolidaySelect.executeQuery()) {
							while (rs.next()) {
								int month = rs.getInt(1);
								String holidayString = rs.getString(2);
								yearDataDB.put(new CurrencyCalKeyHolder(keyData.getYear(), keyData.getCountryCode(), month),
										holidayString);
							}
						}
					
				} catch (SQLException e) {
					ErrorHandler.logError("QUERY-ERROR-003", "Select from STTM_CCY_HOLIDAY Failed");
					ApplicationLogger.error(ERROR_LOGGER, "SELECT month, holiday_list FROM STTM_CCY_HOLIDAY WHERE year="
							+ keyData.getYear() + " Currency: " + countryCodeCurrencyMap.get(keyData.getCountryCode()));
					ApplicationLogger.fatal(ERROR_LOGGER, "Failed in STTM_CCY_HOLIDAY", e);
				}
			});
		else if(keyData.getCountryCode().equals("##")) {
			try (PreparedStatement sttmLclHolidaySelect = connection.prepareStatement(query)) {
				
				eurCurrency = "EUR";
				sttmLclHolidaySelect.setString(1, eurCurrency);
				sttmLclHolidaySelect.setInt(2, keyData.getYear());
				try (ResultSet rs = sttmLclHolidaySelect.executeQuery()) {
					while (rs.next()) {
						int month = rs.getInt(1);
						String holidayString = rs.getString(2);
						yearDataDB.put(new CurrencyCalKeyHolder(keyData.getYear(), keyData.getCountryCode(), month),
								holidayString);
					}
				}
			
			} catch (SQLException e) {
				ErrorHandler.logError("QUERY-ERROR-003", "Select from STTM_CCY_HOLIDAY Failed");
				ApplicationLogger.error(ERROR_LOGGER, "SELECT month, holiday_list FROM STTM_CCY_HOLIDAY WHERE year="
						+ keyData.getYear() + " Currency: " + countryCodeCurrencyMap.get(keyData.getCountryCode()));
				ApplicationLogger.fatal(ERROR_LOGGER, "Failed in STTM_CCY_HOLIDAY", e);
			}
		}
		return yearDataDB;
	}

	public void uploadCurrencyHolidays(Map<CurrencyCalKeyHolder, String> updatedData) {
		String query = "UPDATE STTM_CCY_HOLIDAY SET HOLIDAY_LIST = ? WHERE CCY=? AND YEAR = ? AND MONTH = ?";
		updatedData.forEach((keyData, holidayString) -> {
			List<String> currencyList = countryCodeCurrencyMap.get(keyData.getCountryCode());
			if(currencyList != null)
				currencyList.forEach((currency) -> {
					try (PreparedStatement pmtmNetworkHoliday = connection.prepareStatement(query)) {
						
							pmtmNetworkHoliday.setString(1, holidayString);
							pmtmNetworkHoliday.setString(2, currency);
							pmtmNetworkHoliday.setInt(3, keyData.getYear());
							pmtmNetworkHoliday.setInt(4, keyData.getMonth());
							pmtmNetworkHoliday.executeUpdate();
						
					}
		
					catch (SQLException e) {
						ErrorHandler.logError("QUERY-ERROR-004", "UPDATE STTM_CCY_HOLIDAY Failed");
						ApplicationLogger.error(ERROR_LOGGER,
								"UPDATE STTM_CCY_HOLIDAY SET HOLIDAY_LIST =" + holidayString + "WHERE YEAR = "
										+ keyData.getYear() + " AND MONTH = " + keyData.getMonth() + "Currency: "
										+ countryCodeCurrencyMap.get(keyData.getCountryCode()));
						ApplicationLogger.fatal(ERROR_LOGGER, "Failed in STTM_CCY_HOLIDAY UPDATE", e);
					}
				});
			else if(keyData.getCountryCode().equals("##")) {
				try (PreparedStatement pmtmNetworkHoliday = connection.prepareStatement(query)) {
					
					final String eurCurrency = "EUR";
					pmtmNetworkHoliday.setString(1, holidayString);
					pmtmNetworkHoliday.setString(2, eurCurrency);
					pmtmNetworkHoliday.setInt(3, keyData.getYear());
					pmtmNetworkHoliday.setInt(4, keyData.getMonth());
					pmtmNetworkHoliday.executeUpdate();
				
			}

			catch (SQLException e) {
				ErrorHandler.logError("QUERY-ERROR-004", "UPDATE STTM_CCY_HOLIDAY Failed");
				ApplicationLogger.error(ERROR_LOGGER,
						"UPDATE STTM_CCY_HOLIDAY SET HOLIDAY_LIST =" + holidayString + "WHERE YEAR = "
								+ keyData.getYear() + " AND MONTH = " + keyData.getMonth() + "Currency: "
								+ countryCodeCurrencyMap.get(keyData.getCountryCode()));
				ApplicationLogger.fatal(ERROR_LOGGER, "Failed in STTM_CCY_HOLIDAY UPDATE", e);
			}
			}
		});
	}

}
