package com.oracle.tools.upload.calendar.tasks.enlosers;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.PERFORMANCE_LOGGER;
import static com.oracle.tools.upload.calendar.logging.LoggingFileType.APPLICATION_LOGGER;

import java.util.List;
import org.springframework.beans.factory.BeanNameAware;

import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import com.oracle.tools.upload.calendar.threadProvider.ListThreadExecutor;

import lombok.Getter;
import lombok.Setter;

public abstract class AbstractTask implements ITask, BeanNameAware {
	@Setter
	private List<ITask> parallelTasks;
	@Setter
	@Getter
	private List<ITask> taskList;
	@Getter
	private String beanName;

	@Override
	public boolean execute() {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, beanName + " Enter");
		ApplicationLogger.trace(APPLICATION_LOGGER, beanName + "Start execute of a task");
		ApplicationLogger.trace(APPLICATION_LOGGER, beanName + "Calling process method");
		Boolean status = process();
		ApplicationLogger.trace(APPLICATION_LOGGER, beanName + "Completed process method");
		if (status) {
			if (taskList != null && taskList.size() > 0) {
				for (ITask iTask : taskList) {
					if (status)
						status = iTask.execute();
				}
			}
			executeParallelTasks();
			ApplicationLogger.trace(PERFORMANCE_LOGGER, beanName + " Exit");
		}
		return status;
	}

	private boolean executeParallelTasks() {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, beanName + " Enter");
		boolean response = true;
		if (parallelTasks != null && parallelTasks.size() > 0) {
			ApplicationLogger.trace(APPLICATION_LOGGER, beanName + "Executing Parallel Tasks: " + parallelTasks.size());
			ListThreadExecutor listThreadExecutor = new ListThreadExecutor();
			response = listThreadExecutor.startExecution(parallelTasks);
		}
		ApplicationLogger.trace(PERFORMANCE_LOGGER, beanName + " Exit");
		return response;
	}

	@Override
	public void setBeanName(String name) {
		this.beanName = name;
	}

	protected abstract boolean process();
}
