package com.oracle.tools.upload.calendar.FileSplitter;

import java.nio.file.Path;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;

public class NoFileSplit implements IFileSplitter {

	@Override
	public boolean splitFile(Path file) {
		boolean fileSplitResult = true;
		String fileName = file.getFileName().toString();
		ApplicationConstants.setValue("FILE_NAME", fileName);
		return fileSplitResult;
	}

}
