package com.oracle.tools.upload.calendar.tasks;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Setter;

@Setter
public class CopyFileToDir extends AbstractTask {
	private String sourceRef, destinationRef, basePathRef, sourceFile, destinationDir,
			errorFileCopyFailed = "GEN_FILE_COPY_FAILED";

	@Override
	public boolean process() {
		boolean copyResult = true;
		sourceFile = ApplicationConstants.getValue(sourceFile);
		destinationDir = ApplicationConstants.getValue(destinationDir);
		sourceFile = getSource();
		destinationDir = getDestination();
		updatePaths();
		if (sourceFile != null && destinationDir != null) {
			File src = new File(sourceFile);
			File dest = new File(destinationDir);
			try {
				FileUtils.copyFileToDirectory(src, dest);
				Path path = Paths.get(sourceFile);
				ApplicationConstants.setValue("NEW_SOURCE_FILE", destinationDir + File.separator + path.getFileName().toString());
			} catch (Exception e) {
				copyResult = false;
				ApplicationLogger.fatal(ERROR_LOGGER, "File Copy Failed",e);
				ErrorHandler.logError(errorFileCopyFailed);
			}
		}
		return copyResult;
	}

	private String getDestination() {
		if (destinationDir == null)
			destinationDir = ApplicationConstants.getValue(destinationRef);
		return destinationDir;
	}

	private String getSource() {
		if (sourceFile == null)
			sourceFile = ApplicationConstants.getValue(sourceRef);
		return sourceFile;
	}

	private void updatePaths() {
		if (basePathRef != null && !basePathRef.trim().isEmpty()) {
			sourceFile = ApplicationConstants.getValue(basePathRef) + FileSystems.getDefault().getSeparator() + sourceRef;
			destinationDir = ApplicationConstants.getValue(basePathRef) + FileSystems.getDefault().getSeparator() + destinationRef;
		}

	}
}
