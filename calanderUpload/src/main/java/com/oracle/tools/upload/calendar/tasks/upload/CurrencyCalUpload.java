package com.oracle.tools.upload.calendar.tasks.upload;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oracle.tools.upload.calendar.connection.database.IConnectionProvider;
import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.database.operations.CurrencyCalOperations;
import com.oracle.tools.upload.calendar.dataholders.CurrencyCalDataHolder;
import com.oracle.tools.upload.calendar.dataholders.CurrencyCalKeyHolder;
import com.oracle.tools.upload.calendar.parser.CurrencyFileParser;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Setter;

public class CurrencyCalUpload extends AbstractTask {
	@Setter
	IConnectionProvider connectionProvider, connectionProviderENTITY;
	@Setter
	String invalidFileData;
	CurrencyCalOperations currencyCalOperations;

	@Override
	protected boolean process() {
		boolean processResult = true;
		if (ApplicationConstants.getValue("ARGS[3]").equals("CurrencyCalendar")) {
			Map<CurrencyCalKeyHolder, List<CurrencyCalDataHolder>> fileData = null;
			CurrencyFileParser fileParser = new CurrencyFileParser();
			Connection connection = null;
			Connection connectionEntity = null;
			try {
				connection = connectionProvider.getConnection();
				connectionEntity = connectionProviderENTITY.getConnection();
			} catch (Exception e) {
				processResult = false;
			}
			if (processResult) {
				currencyCalOperations = new CurrencyCalOperations(connection, connectionEntity);
				fileParser.setInvalidFileData(invalidFileData);
				processResult = fileParser.parseFile();
				if (processResult)
					fileData = fileParser.getParsedData();
				if (!fileData.isEmpty()) {
					processResult = currencyCalOperations.createYearData(fileData.keySet());
					upload(fileData);
				}
			}
		}
		return processResult;
	}

	private void upload(Map<CurrencyCalKeyHolder, List<CurrencyCalDataHolder>> fileData) {
		Map<CurrencyCalKeyHolder, String> updatedData = new HashMap<CurrencyCalKeyHolder, String>();
		fileData.forEach((keyData, dataList) -> {
			Map<CurrencyCalKeyHolder, String> yearDataDB = currencyCalOperations.getMonthlyData(keyData);
			dataList.forEach((data) -> {
				StringBuffer monthString;
				CurrencyCalKeyHolder currencyCalKeyHolder = new CurrencyCalKeyHolder(keyData.getYear(),
						keyData.getCountryCode(), data.getMonth());
				if (updatedData.get(currencyCalKeyHolder) == null) {
					updatedData.put(currencyCalKeyHolder, yearDataDB.get(currencyCalKeyHolder));
				}
				monthString = new StringBuffer(updatedData.get(currencyCalKeyHolder));
				monthString.setCharAt(data.getDate() - 1, data.getHolidayType());
				updatedData.put(currencyCalKeyHolder, monthString.toString());
			});
		});
		currencyCalOperations.uploadCurrencyHolidays(updatedData);
	}

}
