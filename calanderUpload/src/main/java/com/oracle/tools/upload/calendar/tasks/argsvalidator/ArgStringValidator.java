package com.oracle.tools.upload.calendar.tasks.argsvalidator;

import java.util.List;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import com.oracle.tools.upload.calendar.logging.LoggingFileType;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ArgStringValidator extends AbstractTask {
	String error = "UNEXPECTED_INPUT", refValue;
	List<String> expectedValues;

	@Override
	public boolean process() {
		ApplicationLogger.trace(LoggingFileType.PERFORMANCE_LOGGER, "Start");
		String stringValue = ApplicationConstants.getValue(refValue);
		boolean returnValue = true;
		if (expectedValues != null) {
			if (!expectedValues.contains(stringValue)) {
				ApplicationLogger.error(LoggingFileType.ERROR_LOGGER, "Input Parameter " + stringValue + " Expected Values: " + expectedValues);
				ErrorHandler.logError(error);
				returnValue = false;
			}
		}
		ApplicationLogger.trace(LoggingFileType.PERFORMANCE_LOGGER, "Exit");
		return returnValue;
	}

}
