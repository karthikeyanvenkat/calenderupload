package com.oracle.tools.upload.calendar.dataholders;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter @AllArgsConstructor
public class NetworkCalKeyHolder {
	private int year;
	private String networkCode;
	private int month = 0;
	
	public NetworkCalKeyHolder(String networkCode, int year) {
		this.year = year;
		this.networkCode = networkCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + month;
		result = prime * result + ((networkCode == null) ? 0 : networkCode.hashCode());
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NetworkCalKeyHolder other = (NetworkCalKeyHolder) obj;
		if (month != other.month)
			return false;
		if (networkCode == null) {
			if (other.networkCode != null)
				return false;
		} else if (!networkCode.equals(other.networkCode))
			return false;
		if (year != other.year)
			return false;
		return true;
	}
	
	
}
