package com.oracle.tools.upload.calendar.tasks;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.PERFORMANCE_LOGGER;

import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

public class MainTask extends AbstractTask {

	@Override
	protected boolean process() {
		String beanName  = this.getBeanName();
		ApplicationLogger.trace(PERFORMANCE_LOGGER, beanName + " Start");
		ApplicationLogger.trace(PERFORMANCE_LOGGER, beanName + " No Process Here");
		ApplicationLogger.trace(PERFORMANCE_LOGGER, beanName + " END");
		return true;
	}

}
