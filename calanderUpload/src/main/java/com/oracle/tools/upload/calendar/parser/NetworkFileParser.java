package com.oracle.tools.upload.calendar.parser;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.dataholders.NetworkCalDataHolder;
import com.oracle.tools.upload.calendar.dataholders.NetworkCalKeyHolder;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

import lombok.Getter;
import lombok.Setter;

public class NetworkFileParser {
	@Setter
	private String invalidFileData;
	private volatile String errorData;
	@Getter
	Map<NetworkCalKeyHolder, List<NetworkCalDataHolder>> parsedData;
	private List<String> invalidData;
	private String partUploadFailed = "PART_WRONG_FORMAT";

	public boolean parseFile() {
		parsedData = new HashMap<NetworkCalKeyHolder, List<NetworkCalDataHolder>>();
		String fileName = ApplicationConstants.getValue("FILE_NAME");
		String directory = ApplicationConstants.getValue("PREPROCESS_DIR");
		String filePath = directory + File.separator + fileName;
		File f = new File(filePath);
		Path file = f.toPath();
		Charset charset = Charset.defaultCharset();
		if (f.exists()) {
			try {
				Files.lines(file, charset).forEach(line -> {
					if (line != null && !line.isEmpty() && (line.charAt(31) == 'W' || line.charAt(31) == 'H')) {
						try {
							NetworkCalKeyHolder keyDataHolder;
							int year = 0;
							NetworkCalDataHolder lineData = new NetworkCalDataHolder();
							String networkCode = line.substring(0, 15).trim();
							lineData.setHostCode(line.substring(15, 23).trim());
							year = Integer.parseInt(line.substring(23, 27).trim());
							keyDataHolder = new NetworkCalKeyHolder(networkCode, year);
							lineData.setMonth(Integer.parseInt(line.substring(27, 29).trim()));
							lineData.setDate(Integer.parseInt(line.substring(29, 31).trim()));
							lineData.setHolidayType(line.charAt(31));
							if (parsedData.get(keyDataHolder) == null) {
								parsedData.put(keyDataHolder, new ArrayList<NetworkCalDataHolder>());
							}
							parsedData.get(keyDataHolder).add(lineData);
						} catch (Exception e) {
							errorData = line;
							if(invalidData == null)
								invalidData = new ArrayList<String>();
							invalidData.add(line);
							//throw e; /*Commented this line to process valid lines in Upload file */
						}
					}else if(!(line.charAt(31) == 'W' || line.charAt(31) == 'H')) {
						if(invalidData == null)
							invalidData = new ArrayList<String>();
						invalidData.add(line);
					}
				});
			} catch (IOException e) {
				ApplicationLogger.fatal(ERROR_LOGGER, "Failed while parsing data: " + errorData, e);
				ErrorHandler.logError(invalidFileData);
				//return false; /*Commented this line to process valid lines in Upload file */
			}
		}
		if(invalidData != null) {
			ErrorHandler.logError(this.partUploadFailed);
		}
		return true;
	}
}
