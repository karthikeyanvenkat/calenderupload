package com.oracle.tools.upload.calendar.tasks.enlosers;

public interface ITask {
	public boolean execute();
}
