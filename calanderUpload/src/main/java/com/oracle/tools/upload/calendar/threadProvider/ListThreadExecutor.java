package com.oracle.tools.upload.calendar.threadProvider;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.APPLICATION_LOGGER;
import static com.oracle.tools.upload.calendar.logging.LoggingFileType.PERFORMANCE_LOGGER;
import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;

import org.springframework.context.ApplicationContext;

import com.oracle.tools.upload.calendar.context.ApplicationContextProvider;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import com.oracle.tools.upload.calendar.tasks.enlosers.ITask;

public class ListThreadExecutor/* implements ApplicationContextAware */ {
	private ApplicationContext applicationContext;
//	private boolean executionResult = true;

	public boolean startExecution(List<ITask> taskList) {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, " Enter");
		boolean executionResult = true;
		if (taskList.size() == 1) {
			ApplicationLogger.trace(APPLICATION_LOGGER, "Executing Single Task : " + taskList.get(0));
			executionResult = taskList.get(0).execute();
		} else if (taskList.size() > 0) {
			executionResult = executeAll(taskList);
		}
		ApplicationLogger.trace(PERFORMANCE_LOGGER, " Exit");
		return executionResult;
	}

	private boolean executeAll(List<ITask> taskList) {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, " Enter");
		applicationContext = ApplicationContextProvider.getContext();
		ExecutorStore executorStore = (ExecutorStore) applicationContext.getBean("executor");
		ExecutorService executorService = executorStore.getExecutorService();
		List<CompletableFuture<Boolean>> resultList = new ArrayList<CompletableFuture<Boolean>>();
		for (ITask task : taskList) {
			CompletableFuture<Boolean> completableFuture = CompletableFuture.supplyAsync(() -> {
				boolean executionResult = true;
				try {
					executionResult = task.execute();
				} catch (Exception exception) {
					ErrorHandler.logError("FATAL-005", "Unhandled Exception in Thread execution");
					ApplicationLogger.fatal(ERROR_LOGGER, "Unhandled Exception in Thread execution", exception);
					executionResult = false;
				}
				return executionResult;
			}, executorService);
			resultList.add(completableFuture);
		}
		for (CompletableFuture<Boolean> future : resultList) {
			try {
				if (!future.get())
					return false;
			} catch (InterruptedException exception) {
				ErrorHandler.logError("THREAD-ERROR-001", "Thread Interrupted");
				ApplicationLogger.fatal(ERROR_LOGGER, "Thread Interrupted", exception);
				return false;
			} catch (ExecutionException exception) {
				ErrorHandler.logError("THREAD-ERROR-002", "Thread ExecutionException");
				ApplicationLogger.fatal(ERROR_LOGGER, "Thread ExecutionException", exception);
				return false;
			}
		}
		ApplicationLogger.trace(PERFORMANCE_LOGGER, " Exit");
		return true;
	}

}
