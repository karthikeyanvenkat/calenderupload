package com.oracle.tools.upload.calendar.tasks.argsvalidator;

import java.io.File;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import com.oracle.tools.upload.calendar.logging.LoggingFileType;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Setter;

@Setter
public class ArgDirValidator extends AbstractTask {
	String directoryNotExist = "GENRAL_DIR_NOT_EXIST", isNotDirectory = "GENRAL_NOT_A_DIR",	noWritePermissions = "GENERAL_DIR_PERMISSIONS_ISSUE", refValue;

	@Override
	protected boolean process() {
		ApplicationLogger.trace(LoggingFileType.PERFORMANCE_LOGGER, "Start");
		String dirPath = ApplicationConstants.getValue(refValue);
		boolean fileValidation = false;
		if (dirPath != null) {
			File d = new File(dirPath);
			if (d.exists() && d.isDirectory() && d.canWrite()) {
				ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER,
						"Directory " + dirPath + " basic validation success");
				fileValidation = true;
			} else if (!d.exists()) {
				ErrorHandler.logError(directoryNotExist);
				ApplicationLogger.error(LoggingFileType.ERROR_LOGGER, "Directory " + dirPath + " does not exist");
			} else if (!d.isDirectory()) {
				ApplicationLogger.error(LoggingFileType.ERROR_LOGGER,
						"Directory " + dirPath + " is not Directory");
				ErrorHandler.logError(isNotDirectory);
			} else if (!d.canWrite()) {
				ApplicationLogger.error(LoggingFileType.ERROR_LOGGER,
						"File " + dirPath + " do not have write permissions");
				ErrorHandler.logError(noWritePermissions);
			}
		}
		ApplicationLogger.trace(LoggingFileType.PERFORMANCE_LOGGER, "Start");
		return fileValidation;
	}

}
