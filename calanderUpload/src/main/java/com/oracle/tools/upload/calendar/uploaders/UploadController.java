package com.oracle.tools.upload.calendar.uploaders;

import com.oracle.tools.upload.calendar.connection.database.IConnectionProvider;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Setter;

public class UploadController extends AbstractTask{
	@Setter
	String filePath, fileNAmePrefix;
	@Setter 
	IConnectionProvider connection;
	@Override
	protected boolean process() {
		return false;
	}

}
