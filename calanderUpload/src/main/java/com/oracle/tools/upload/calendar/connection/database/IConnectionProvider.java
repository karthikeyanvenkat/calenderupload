package com.oracle.tools.upload.calendar.connection.database;

import java.sql.Connection;

public interface IConnectionProvider {
	public Connection getConnection() throws Exception;
}
