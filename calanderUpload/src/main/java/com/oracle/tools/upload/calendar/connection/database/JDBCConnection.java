package com.oracle.tools.upload.calendar.connection.database;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.sql.Connection;
import java.sql.SQLException;

import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

import lombok.Setter;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

public class JDBCConnection implements IConnectionProvider {
	@Setter
	private String host, port, userName, password, serviceName;
	@Setter
	private Integer minPoolSize, maxPoolSize, initialPoolSize;
	private PoolDataSource pds;

	@Override
	public Connection getConnection() throws Exception {
		Connection connection = null;
		if (pds == null)
			createPooledDatasource();
		try {
			connection = pds.getConnection();
		} catch (SQLException exception) {
			ErrorHandler.logError("DB-ERROR-001","Failed in getting connection from pooled Datasource");
			ApplicationLogger.error(ERROR_LOGGER, "host: " + host);
			ApplicationLogger.error(ERROR_LOGGER, "port: " + port);
			ApplicationLogger.error(ERROR_LOGGER, "userName: " + userName);
			ApplicationLogger.error(ERROR_LOGGER, "serviceName: " + serviceName);
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in getting connection from pooled Datasource" , exception);
		}
		return connection;
	}

	private synchronized void createPooledDatasource() {
		if (pds == null) {
			pds = PoolDataSourceFactory.getPoolDataSource();
			String connectURL = "jdbc:oracle:thin:@//" + host + ":" + port + "/" + serviceName;
			try {
				pds.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
				pds.setURL(connectURL);
				pds.setUser(userName);
				pds.setPassword(password);
				pds.setInitialPoolSize(initialPoolSize);
				pds.setMinPoolSize(minPoolSize);
				pds.setMaxPoolSize(maxPoolSize);
				pds.setValidateConnectionOnBorrow(true);
				pds.setSQLForValidateConnection("select 1 from DUAL");
			} catch (SQLException exception) {
				ErrorHandler.logError("DB-ERROR-002","Failed in creating pooled Datasource");
				ApplicationLogger.error(ERROR_LOGGER, "host: " + host);
				ApplicationLogger.error(ERROR_LOGGER, "port: " + port);
				ApplicationLogger.error(ERROR_LOGGER, "userName: " + userName);
				ApplicationLogger.error(ERROR_LOGGER, "serviceName: " + serviceName);
				ApplicationLogger.fatal(ERROR_LOGGER, "Failed in creating pooled Datasource" , exception);
			}
		}
	}

}
