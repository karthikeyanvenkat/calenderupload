package com.oracle.tools.upload.calendar.constants;

import java.util.Map;

import lombok.Setter;

@Setter
public class UploadUsers {
	private Map<String, String> uploadUsersMap;
	
	public String getUser(String key)
	{
		return uploadUsersMap.get(key);
	}
}
