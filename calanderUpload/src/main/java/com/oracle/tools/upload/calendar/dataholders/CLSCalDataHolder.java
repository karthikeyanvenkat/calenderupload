package com.oracle.tools.upload.calendar.dataholders;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CLSCalDataHolder {
	private int date, month;
	private char holidayType;
}
