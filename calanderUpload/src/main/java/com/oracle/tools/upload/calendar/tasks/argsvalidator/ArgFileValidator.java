package com.oracle.tools.upload.calendar.tasks.argsvalidator;

import java.io.File;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import com.oracle.tools.upload.calendar.logging.LoggingFileType;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Setter;

@Setter
public class ArgFileValidator extends AbstractTask {
	private String fileNotExist = "GENRAL_FILE_NOT_EXIST", isNotFile = "GENRAL_NOT_A_FILE",
			nonReadableFormat = "GENERAL_FILE_PERMISSIONS_ISSUE", refValue;

	@Override
	public boolean process() {
		ApplicationLogger.trace(LoggingFileType.PERFORMANCE_LOGGER, "Start");
		boolean fileValidation = false;
		String filePath = ApplicationConstants.getValue(refValue);
		File f = new File(filePath);
		if (f.exists() && f.isFile() && f.canRead()) {
			ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER,
					"File " + filePath + " basic validation success");
			fileValidation = true;
		} else if (!f.exists()) {
			ErrorHandler.logError(fileNotExist);
			ApplicationLogger.error(LoggingFileType.ERROR_LOGGER, "File " + filePath + " does not exist");
		} else if (!f.isFile()) {
			ApplicationLogger.error(LoggingFileType.ERROR_LOGGER, "File " + filePath + " is not a file");
			ErrorHandler.logError(isNotFile);
		} else if (!f.canRead()) {
			ApplicationLogger.error(LoggingFileType.ERROR_LOGGER, "File " + filePath + " is not readable");
			ErrorHandler.logError(nonReadableFormat);
		}
		ApplicationLogger.trace(LoggingFileType.PERFORMANCE_LOGGER, "Start");
		return fileValidation;
	}

}
