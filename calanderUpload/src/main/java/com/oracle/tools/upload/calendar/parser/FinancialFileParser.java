package com.oracle.tools.upload.calendar.parser;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.dataholders.FinancialCalDataHolder;
import com.oracle.tools.upload.calendar.dataholders.FinancialCalKeyHolder;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

import lombok.Getter;
import lombok.Setter;

public class FinancialFileParser {
	@Setter
	private String invalidFileData;
	private volatile String errorData;
	@Getter
	Map<FinancialCalKeyHolder, List<FinancialCalDataHolder>> parsedData;
	private List<String> invalidData;
	private String partUploadFailed = "PART_WRONG_FORMAT";

	public boolean parseFile() {
		parsedData = new HashMap<FinancialCalKeyHolder, List<FinancialCalDataHolder>>();
		String fileName = ApplicationConstants.getValue("FILE_NAME");
		String directory = ApplicationConstants.getValue("PREPROCESS_DIR");
		String filePath = directory + File.separator + "MERGED" + fileName;
		File f = new File(filePath);
		Path file = f.toPath();
		Charset charset = Charset.defaultCharset();
		if (f.exists()) {
			try {
				Files.lines(file, charset).forEach(line -> {
					if (line != null && !line.isEmpty() && (line.charAt(11) == 'W' || line.charAt(11) == 'H')) {
						try {
							FinancialCalKeyHolder keyDataHolder;
							int year = 0;
							FinancialCalDataHolder lineData = new FinancialCalDataHolder();
							String clearingHouseCode = line.substring(0, 3).trim();
							year = Integer.parseInt(line.substring(3, 7).trim());
							keyDataHolder = new FinancialCalKeyHolder(clearingHouseCode, year);
							lineData.setMonth(Integer.parseInt(line.substring(7, 9).trim()));
							lineData.setDate(Integer.parseInt(line.substring(9, 11).trim()));
							lineData.setHolidayType(line.charAt(11));
							if (parsedData.get(keyDataHolder) == null) {
								parsedData.put(keyDataHolder, new ArrayList<FinancialCalDataHolder>());
							}
							parsedData.get(keyDataHolder).add(lineData);
						} catch (Exception e) {
							errorData = line;
							if(invalidData == null)
								invalidData = new ArrayList<String>();
							invalidData.add(line);
							//throw e; /*Commented this line to process valid lines in Upload file */
						}
					}else if(!(line.charAt(11) == 'W' || line.charAt(11) == 'H')) {
						if(invalidData == null)
							invalidData = new ArrayList<String>();
						invalidData.add(line);
					}
				});
			} catch (IOException e) {
				ApplicationLogger.fatal(ERROR_LOGGER, "Failed while parsing data: " + errorData, e);
				ErrorHandler.logError(invalidFileData);
				//return false; /*Commented this line to process valid lines in Upload file */
			}
		}
		if(invalidData != null) {
			ErrorHandler.logError(this.partUploadFailed);
		}
		return true;
	}
}
