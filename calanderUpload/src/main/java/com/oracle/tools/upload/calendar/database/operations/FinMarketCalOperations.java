package com.oracle.tools.upload.calendar.database.operations;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.oracle.tools.upload.calendar.constants.UploadUsers;
import com.oracle.tools.upload.calendar.context.ApplicationContextProvider;
import com.oracle.tools.upload.calendar.dataholders.FinancialCalKeyHolder;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

public class FinMarketCalOperations {
	UploadUsers users;
	Connection connection;
	Set<String> clearingHouseSet;

	public FinMarketCalOperations(Connection connection) {
		this.connection = connection;
	}

	public boolean createYearData(Set<FinancialCalKeyHolder> keySet) {
		users = (UploadUsers) ApplicationContextProvider.getContext().getBean("users");
		clearingHouseSet = new HashSet<String>();
		try {
			keySet.forEach((keyData) -> {
				String clearingHouseCode = keyData.getClearingHouseCode();
				if (!clearingHouseSet.contains(clearingHouseCode + keyData.getYear())) {
					clearingHouseSet.add(clearingHouseCode + keyData.getYear());
					try (PreparedStatement statement = connection.prepareStatement(
							"SELECT COUNT(1) FROM TRTM_FIC_HOL_MASTER WHERE FINANCIAL_CENTER=? and YEAR=?")) {
						statement.setString(1, clearingHouseCode);
						statement.setInt(2, keyData.getYear());
						try (ResultSet rs = statement.executeQuery()) {
							while (rs.next()) {
								int resultCount = rs.getInt(1);
								if (resultCount == 0)
									if (!createEntry(keyData.getYear(), keyData.getClearingHouseCode()))
										throw new Exception(
												"Failed in creating monthly data. createEntry returned false for Year="
														+ keyData.getYear() + " Financial Center: "
														+ clearingHouseCode);
							}
						}
					} catch (Exception exception) {
						ErrorHandler.logError("QUERY-ERROR-001",
								"Failed for year : " + keyData.getYear() + "Financial Center: " + clearingHouseCode);
						ApplicationLogger.fatal(ERROR_LOGGER,
								"Failed for " + keyData.getYear() + "Financial Center: " + clearingHouseCode,
								exception);
						throw new RuntimeException(exception);
					}
				}
			});
		} catch (Exception exception) {
			return false;
		}
		return true;
	}
	
	public boolean validatePDBCount(int size) {
		boolean processResult = true;
		try (ResultSet rs = connection.createStatement().executeQuery("SELECT COUNT(1) FROM SMTB_ENTITY_DETAILS")) {
			while (rs.next()) {
				int resultCount = rs.getInt(1);
				if (resultCount != size)
					processResult = false;
			}
		} catch (Exception exception) {
			ErrorHandler.logError("QUERY-ERROR-007", "Failed for SMTB_ENTITY_DETAILS COUNT");
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed for SMTB_ENTITY_DETAILS COUNT", exception);
			processResult = false;
		}
		return processResult;
	}
	
	public List<String> getFinCentres() throws Exception {
		List<String> finCentersList = new ArrayList<String>();
		try (PreparedStatement statement = connection.prepareStatement("SELECT financial_center FROM trtm_fin_defn where record_stat='O' and auth_stat='A'")) {
			try (ResultSet rs = statement.executeQuery()) {
				while (rs.next()) {
					finCentersList.add(rs.getString(1));
				}
			}
		} catch (Exception exception) {
			ErrorHandler.logError("QUERY-ERROR-008", "Failed: SELECT financial_center FROM trtm_fin_defn where record_stat='O' and auth_stat='A'");
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed: SELECT financial_center FROM trtm_fin_defn where record_stat='O' and auth_stat='A'", exception);
			throw exception;
		}
		return finCentersList;
	}

	private boolean createEntry(int year, String clearingHouseCode) {
		Calendar calendar = Calendar.getInstance();
		String fullWorkingString = "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW";
		String query = "INSERT INTO TRTM_FIC_HOL_MASTER(ONCE_AUTH, MAKER_ID, MAKER_DT_STAMP, CHECKER_ID, CHECKER_DT_STAMP, RECORD_STAT, MOD_NO, AUTH_STAT, YEAR, FINANCIAL_CENTER, WEEKLY_HOLIDAYS, UNEXP_HOL)"
				+ "VALUES('Y',?,CURRENT_DATE,?,CURRENT_DATE ,'O', 1, 'A',?,?, null, null)";
		StringBuffer monthString = null;
		int month = 0;
		try (PreparedStatement sttmLclHolMasterInsert = connection.prepareStatement(query)) {
			sttmLclHolMasterInsert.setString(1, users.getUser("MAKER_ID"));
			sttmLclHolMasterInsert.setString(2, users.getUser("CHECKER_ID"));
			sttmLclHolMasterInsert.setInt(3, year);
			sttmLclHolMasterInsert.setString(4, clearingHouseCode);
			boolean insertedSuccessfully = sttmLclHolMasterInsert.execute();
			for (month = 1; month <= 12; month++) {
				// Get the number of days in that month
				YearMonth yearMonthObject = YearMonth.of(year, month);
				int daysInMonth = yearMonthObject.lengthOfMonth();
				monthString = new StringBuffer(fullWorkingString.substring(0, daysInMonth));
				for (int day = 1; day <= daysInMonth; day++) {
					calendar.set(year, month - 1, day);
					int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
					if (dayOfWeek == Calendar.SUNDAY || dayOfWeek == Calendar.SATURDAY) {
						monthString.setCharAt(day - 1, 'H');
					}
				}

				try (PreparedStatement sttmLclHolidayInsert = connection
						.prepareStatement("INSERT INTO TRTM_FIC_HOLIDAY(FINANCIAL_CENTER, YEAR, MONTH, HOLIDAY_LIST)"
								+ "VALUES(?,?,?,?)")) {
					sttmLclHolidayInsert.setString(1, clearingHouseCode);
					sttmLclHolidayInsert.setInt(2, year);
					sttmLclHolidayInsert.setInt(3, month);
					sttmLclHolidayInsert.setString(4, monthString.toString());
					insertedSuccessfully = sttmLclHolidayInsert.execute();
				}
			}

		} catch (Exception e) {
			ErrorHandler.logError("QUERY-ERROR-002", "INSERT INTO TRTM_FIC_HOLIDAY Failed");
			ApplicationLogger.error(ERROR_LOGGER, "INSERT INTO TRTM_FIC_HOLIDAY(YEAR, MONTH, HOLIDAY_LIST)");
			ApplicationLogger.error(ERROR_LOGGER, "year: " + year);
			ApplicationLogger.error(ERROR_LOGGER, "month: " + month);
			ApplicationLogger.error(ERROR_LOGGER, "monthString: " + monthString);
			ApplicationLogger.error(ERROR_LOGGER, "Finance Centre: " + clearingHouseCode);
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in TRTM_FIC_HOLIDAY INSERT", e);
			return false;
		}
		return true;
	}

	public Map<FinancialCalKeyHolder, String> getMonthlyData(FinancialCalKeyHolder keyData) {
		Map<FinancialCalKeyHolder, String> yearDataDB = new HashMap<FinancialCalKeyHolder, String>();
		String query = "SELECT month, holiday_list FROM TRTM_FIC_HOLIDAY WHERE financial_center=? AND year=?";
		try (PreparedStatement sttmLclHolidaySelect = connection.prepareStatement(query)) {
			sttmLclHolidaySelect.setString(1, keyData.getClearingHouseCode());
			sttmLclHolidaySelect.setInt(2, keyData.getYear());
			try (ResultSet rs = sttmLclHolidaySelect.executeQuery()) {
				while (rs.next()) {
					int month = rs.getInt(1);
					String holidayString = rs.getString(2);
					yearDataDB.put(new FinancialCalKeyHolder(keyData.getYear(), keyData.getClearingHouseCode(), month),
							holidayString);
				}
			}
		} catch (SQLException e) {
			ErrorHandler.logError("QUERY-ERROR-003", "Select from TRTM_FIC_HOLIDAY Failed");
			ApplicationLogger.error(ERROR_LOGGER, "SELECT month, holiday_list FROM TRTM_FIC_HOLIDAY WHERE year="
					+ keyData.getYear() + " FinancialCenter: " + keyData.getClearingHouseCode());
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in TRTM_FIC_HOLIDAY", e);
		}
		return yearDataDB;
	}

	public void uploadClearingHouseHolidays(Map<FinancialCalKeyHolder, String> updatedData) {
		String query = "UPDATE TRTM_FIC_HOLIDAY SET HOLIDAY_LIST = ? WHERE FINANCIAL_CENTER=? AND YEAR = ? AND MONTH = ?";
		updatedData.forEach((keyData, holidayString) -> {
			try (PreparedStatement sttmLclHolidayUpdate = connection.prepareStatement(query)) {
				sttmLclHolidayUpdate.setString(1, holidayString);
				sttmLclHolidayUpdate.setString(2, keyData.getClearingHouseCode());
				sttmLclHolidayUpdate.setInt(3, keyData.getYear());
				sttmLclHolidayUpdate.setInt(4, keyData.getMonth());
				sttmLclHolidayUpdate.executeUpdate();
			}

			catch (SQLException exception) {
				ErrorHandler.logError("QUERY-ERROR-004", "UPDATE TRTM_FIC_HOLIDAY Failed");
				ApplicationLogger.error(ERROR_LOGGER,
						"UPDATE TRTM_FIC_HOLIDAY SET HOLIDAY_LIST =" + holidayString + "WHERE YEAR = "
								+ keyData.getYear() + " AND MONTH = " + keyData.getMonth() + " AND FIN_CENTRE= "
								+ keyData.getClearingHouseCode());
				ApplicationLogger.fatal(ERROR_LOGGER, "Failed in TRTM_FIC_HOLIDAY UPDATE", exception);
			}
		});
	}

}
