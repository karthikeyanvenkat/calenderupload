package com.oracle.tools.upload.calendar.database.operations;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.oracle.tools.upload.calendar.constants.UploadUsers;
import com.oracle.tools.upload.calendar.context.ApplicationContextProvider;
import com.oracle.tools.upload.calendar.dataholders.BranchCalKeyHolder;
import com.oracle.tools.upload.calendar.error.ApplicationErrorHolder;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

public class BranchCalOperations {

	UploadUsers users;
	Connection connection;
	Set<String> branches;

	public BranchCalOperations(Connection connection) {
		this.connection = connection;
	}

	public boolean createYearData(Set<BranchCalKeyHolder> set) {
		users = (UploadUsers) ApplicationContextProvider.getContext().getBean("users");
		branches = new HashSet<String>();
		try {
			set.forEach((keyData) -> {
				String branchCode = keyData.getBranchCode();
				if (!branches.contains(branchCode + keyData.getYear())) {
					branches.add(branchCode + keyData.getYear());
					try (PreparedStatement statement = connection.prepareStatement(
							"SELECT COUNT(1) FROM STTM_LCL_HOL_MASTER WHERE BRANCH_CODE=? and YEAR=?")) {
						statement.setString(1, branchCode);
						statement.setInt(2, keyData.getYear());
						try (ResultSet rs = statement.executeQuery()) {
							while (rs.next()) {
								int resultCount = rs.getInt(1);
								if (resultCount == 0)
									if (!createEntry(keyData.getYear(), keyData.getBranchCode()))
										throw new Exception(
												"Failed in creating monthly data. createEntry returned false for Branch="
														+ branchCode + " and Year=" + keyData.getYear());
							}
						}
					} catch (Exception exception) {
						ErrorHandler.logError("QUERY-ERROR-001",
								"Failed for branch: " + branchCode + " Year: " + keyData.getYear());
						ApplicationLogger.fatal(ERROR_LOGGER,
								"Failed for branch: " + branchCode + " Year: " + keyData.getYear(), exception);
						throw new RuntimeException(exception);
					}
				}
			});
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private boolean createEntry(Integer year, String branchCode) {
		ApplicationLogger.debug(APPLICATION_LOGGER, "Inside createEntry");
		ApplicationLogger.debug(APPLICATION_LOGGER, "Year: " + year);
		ApplicationLogger.debug(APPLICATION_LOGGER, "branchCode: " + branchCode);
		Calendar calendar = Calendar.getInstance();
		String fullWorkingString = "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW";
		String query = "INSERT INTO STTM_LCL_HOL_MASTER(AUTH_STAT, BRANCH_CODE, YEAR, MOD_NO, RECORD_STAT, WEEKLY_HOLIDAYS ,CHECKER_DT_STAMP,CHECKER_ID, MAKER_DT_STAMP,MAKER_ID, ONCE_AUTH)"
				+ "VALUES('A',?,?,1,'O', 60,CURRENT_DATE , ?,CURRENT_DATE,?,'Y' )";
		StringBuffer monthString = null;
		int month = 0;
		try (PreparedStatement sttmLclHolMasterInsert = connection.prepareStatement(query)) {
			sttmLclHolMasterInsert.setString(1, branchCode);
			sttmLclHolMasterInsert.setInt(2, year);
			sttmLclHolMasterInsert.setString(3, users.getUser("CHECKER_ID"));
			sttmLclHolMasterInsert.setString(4, users.getUser("MAKER_ID"));
			boolean insertedSuccessfully = sttmLclHolMasterInsert.execute();
			for (month = 1; month <= 12; month++) {
				// Get the number of days in that month
				YearMonth yearMonthObject = YearMonth.of(year, month);
				int daysInMonth = yearMonthObject.lengthOfMonth();
				monthString = new StringBuffer(fullWorkingString.substring(0, daysInMonth));
				for (int day = 1; day <= daysInMonth; day++) {
					calendar.set(year, month - 1, day);
					int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
					if (dayOfWeek == Calendar.SUNDAY || dayOfWeek == Calendar.SATURDAY) {
						monthString.setCharAt(day - 1, 'H');
					}
				}

				try (PreparedStatement sttmLclHolidayInsert = connection.prepareStatement(
						"INSERT INTO STTM_LCL_HOLIDAY(BRANCH_CODE, YEAR, MONTH, HOLIDAY_LIST)" + "VALUES(?,?,?,?)")) {
					ApplicationLogger.debug(APPLICATION_LOGGER, "Inserting into STTM_LCL_HOLIDAY");
					ApplicationLogger.debug(APPLICATION_LOGGER, "branchCode" + branchCode);
					ApplicationLogger.debug(APPLICATION_LOGGER, "year" + year);
					ApplicationLogger.debug(APPLICATION_LOGGER, "month" + month);
					ApplicationLogger.debug(APPLICATION_LOGGER, "monthString" + monthString.toString());
					sttmLclHolidayInsert.setString(1, branchCode);
					sttmLclHolidayInsert.setInt(2, year);
					sttmLclHolidayInsert.setInt(3, month);
					sttmLclHolidayInsert.setString(4, monthString.toString());
					sttmLclHolidayInsert.execute();
				}
				catch(Exception e)
				{
					ApplicationLogger.fatal(ERROR_LOGGER, "INSERT INTO STTM_LCL_HOLIDAY(BRANCH_CODE, YEAR, MONTH, HOLIDAY_LIST)"
							+ "VALUES(" + branchCode + "," + year + "," + month + "," + monthString + ")", e);
				}
			}

		} catch (Exception exception) {
			ErrorHandler.logError("QUERY-ERROR-002", "INSERT INTO STTM_LCL_HOLIDAY Failed");
			ApplicationLogger.error(ERROR_LOGGER, "INSERT INTO STTM_LCL_HOLIDAY(BRANCH_CODE, YEAR, MONTH, HOLIDAY_LIST)"
					+ "VALUES(" + branchCode + "," + year + "," + month + "," + monthString + ")");
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in STTM_LCL_HOLIDAY INSERT", exception);
			return false;
		}
		return true;
	}

	public Map<BranchCalKeyHolder, String> getMonthlyData(BranchCalKeyHolder keyData) {
		Map<BranchCalKeyHolder, String> yearDataDB = new HashMap<BranchCalKeyHolder, String>();
		String query = "SELECT month, holiday_list FROM STTM_LCL_HOLIDAY WHERE branch_code=? AND year=?";
		try (PreparedStatement sttmLclHolidaySelect = connection.prepareStatement(query)) {
			sttmLclHolidaySelect.setString(1, keyData.getBranchCode());
			sttmLclHolidaySelect.setInt(2, keyData.getYear());
			try (ResultSet rs = sttmLclHolidaySelect.executeQuery()) {
				while (rs.next()) {
					int month = rs.getInt(1);
					String holidayString = rs.getString(2);
					yearDataDB.put(new BranchCalKeyHolder(keyData.getYear(), keyData.getBranchCode(), month),
							holidayString);
				}
			}
		} catch (SQLException e) {
			ErrorHandler.logError("QUERY-ERROR-003", "Select from STTM_LCL_HOLIDAY Failed");
			ApplicationLogger.error(ERROR_LOGGER, "SELECT month, holiday_list FROM STTM_LCL_HOLIDAY WHERE branch_code="
					+ keyData.getBranchCode() + "AND year=" + keyData.getYear());
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in STTM_LCL_HOLIDAY", e);
		}
		return yearDataDB;
	}

	public boolean uploadBanchHolidays(Map<BranchCalKeyHolder, String> updatedData) {
		String query = "UPDATE STTM_LCL_HOLIDAY SET HOLIDAY_LIST = ? WHERE BRANCH_CODE=? AND YEAR = ? AND MONTH = ?";
		updatedData.forEach((keyData, holidayString) -> {
			try (PreparedStatement sttmLclHolidayUpdate = connection.prepareStatement(query)) {
				sttmLclHolidayUpdate.setString(1, holidayString);
				sttmLclHolidayUpdate.setString(2, keyData.getBranchCode());
				sttmLclHolidayUpdate.setInt(3, keyData.getYear());
				sttmLclHolidayUpdate.setInt(4, keyData.getMonth());
				sttmLclHolidayUpdate.executeUpdate();
			}

			catch (SQLException e) {
				ErrorHandler.logError("QUERY-ERROR-004", "UPDATE STTM_LCL_HOLIDAY Failed");
				ApplicationLogger.error(ERROR_LOGGER,
						"UPDATE STTM_LCL_HOLIDAY SET HOLIDAY_LIST =" + holidayString + "WHERE BRANCH_CODE="
								+ keyData.getBranchCode() + " AND YEAR = " + keyData.getYear() + " AND MONTH = "
								+ keyData.getMonth());
				ApplicationLogger.fatal(ERROR_LOGGER, "Failed in STTM_LCL_HOLIDAY UPDATE", e);
			}
		});
		if (ApplicationErrorHolder.getInstance().getError() != null)
			return false;
		return true;
	}

	public boolean validatePDBCount(int size) {
		boolean processResult = true;
		try (ResultSet rs = connection.createStatement().executeQuery("SELECT COUNT(1) FROM SMTB_ENTITY_DETAILS")) {
			while (rs.next()) {
				int resultCount = rs.getInt(1);
				if (resultCount != size)
					processResult = false;
			}
		} catch (Exception exception) {
			ErrorHandler.logError("QUERY-ERROR-007", "Failed for SMTB_ENTITY_DETAILS COUNT");
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed for SMTB_ENTITY_DETAILS COUNT", exception);
			processResult = false;
		}
		return processResult;
	}

	public List<String> getBranches() throws Exception {
		List<String> branchesList = new ArrayList<String>();
		try (PreparedStatement statement = connection.prepareStatement("SELECT branch_code FROM sttm_branch")) {
			try (ResultSet rs = statement.executeQuery()) {
				while (rs.next()) {
					branchesList.add(rs.getString(1));
				}
			}
		} catch (Exception exception) {
			ErrorHandler.logError("QUERY-ERROR-008", "Failed: SELECT branch_code FROM sttm_branch");
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed: SELECT branch_code FROM sttm_branch", exception);
			throw exception;
		}
		return branchesList;
	}

	public String getHO() throws Exception {
		String headOfficeBranchCode = null;
		try (PreparedStatement statement = connection.prepareStatement("SELECT ho_branch FROM sttm_bank")) {
			try (ResultSet rs = statement.executeQuery()) {
				while (rs.next()) {
					headOfficeBranchCode = rs.getString(1);
				}
			}
		} catch (Exception exception) {
			ErrorHandler.logError("QUERY-ERROR-006", "Failed: SELECT ho_branch FROM sttm_bank");
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed:SELECT ho_branch FROM sttm_bank", exception);
			throw exception;
		}
		return headOfficeBranchCode;
	}
}
