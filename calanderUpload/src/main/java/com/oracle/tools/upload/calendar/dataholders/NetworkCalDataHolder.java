package com.oracle.tools.upload.calendar.dataholders;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class NetworkCalDataHolder {
	private int date, month;
	private char holidayType;
	private String hostCode;
}
