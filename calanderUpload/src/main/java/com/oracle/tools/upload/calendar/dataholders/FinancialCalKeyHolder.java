package com.oracle.tools.upload.calendar.dataholders;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter @AllArgsConstructor
public class FinancialCalKeyHolder {
	private int year;
	private String clearingHouseCode;
	private int month = 0;
	
	public FinancialCalKeyHolder(String clearingHouseCode, int year)
	{
		this.clearingHouseCode = clearingHouseCode;
		this.year = year;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clearingHouseCode == null) ? 0 : clearingHouseCode.hashCode());
		result = prime * result + month;
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinancialCalKeyHolder other = (FinancialCalKeyHolder) obj;
		if (clearingHouseCode == null) {
			if (other.clearingHouseCode != null)
				return false;
		} else if (!clearingHouseCode.equals(other.clearingHouseCode))
			return false;
		if (month != other.month)
			return false;
		if (year != other.year)
			return false;
		return true;
	}
	
}
