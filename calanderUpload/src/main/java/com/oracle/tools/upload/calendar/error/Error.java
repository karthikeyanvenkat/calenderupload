package com.oracle.tools.upload.calendar.error;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Error {
	String errorCode, errorMessage;
	Exception exception;
}
