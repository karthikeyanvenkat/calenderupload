package com.oracle.tools.upload.calendar;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.APPLICATION_LOGGER;
import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;
import static com.oracle.tools.upload.calendar.logging.LoggingFileType.EVENT_LOGGER;
import static com.oracle.tools.upload.calendar.logging.LoggingFileType.PERFORMANCE_LOGGER;

import java.util.concurrent.ExecutorService;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.error.ApplicationErrorHolder;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import com.oracle.tools.upload.calendar.tasks.argsvalidator.ArgBeansFileValidator;
import com.oracle.tools.upload.calendar.tasks.enlosers.ITask;
import com.oracle.tools.upload.calendar.threadProvider.ExecutorStore;

public class CalanderUploadApplication {

	public static void main(String... args) {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "######################################## Performence logs Start ########################################");
		ApplicationLogger.trace(EVENT_LOGGER, "######################################## Event logs Start ########################################");
		ApplicationLogger.trace(APPLICATION_LOGGER, "######################################## Appplication logs Start ########################################");
		ApplicationLogger.trace(ERROR_LOGGER, "######################################## Error logs Start ########################################");
		
		ExecutorService executorService = null;
		int executionStatus = validateFirstArgument(args);
		if (executionStatus == 0) {
			try (AbstractApplicationContext context = new FileSystemXmlApplicationContext("file:" + args[0])) {
				
				ITask mainTask = (ITask) context.getBean("uploadController");
				ExecutorStore executorStore = (ExecutorStore) context.getBean("executor");
				executionStatus = getExecutionStatus(mainTask.execute());
				executorService = executorStore.getExecutorService();
			} catch (Exception exception) {
				executionStatus = 9;
				System.err.print("Failed in execution");
				ApplicationLogger.fatal(ERROR_LOGGER, "Unhandled Exception in execution", exception);
			} finally {
				if (executorService != null && !executorService.isShutdown())
					executorService.shutdown();
			}
		}
		if (ApplicationErrorHolder.getInstance().getError() != null) {
			executionStatus = printError(executionStatus);
		}
		if (executionStatus == 0) {
			ApplicationLogger.info(APPLICATION_LOGGER, "Execution Completed Succressfully");
			System.out.println("Successful completion");
		}
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "######################################## Performence logs End ########################################");
		ApplicationLogger.trace(EVENT_LOGGER, "######################################## Event logs End ########################################");
		ApplicationLogger.trace(APPLICATION_LOGGER, "######################################## Appplication logs End ########################################");
		ApplicationLogger.trace(ERROR_LOGGER, "######################################## Error logs End ########################################");
		System.exit(executionStatus);
	}

	private static int getExecutionStatus(boolean execute) {
		if (execute)
			return 0;
		else
			return 1;
	}

	private static int validateFirstArgument(String[] args) {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "Enter");
		int returnValue = 0;
		setArguments(args);
		ArgBeansFileValidator fileValidator = new ArgBeansFileValidator();
		ApplicationLogger.debug(APPLICATION_LOGGER, "Validating first Argument");
		returnValue = fileValidator.process();
		ApplicationLogger.debug(APPLICATION_LOGGER, "Validation result: " + returnValue);
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "Exit");
		return returnValue;
	}

	private static void setArguments(String[] args) {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "Enter");
		ApplicationLogger.debug(APPLICATION_LOGGER, "Setting Arguments Length as ARGS_LENGTH in ApplicationConstants:" + (args.length) );
		ApplicationConstants.setValue("ARGS_LENGTH", (args.length) + "");
		for (int i = 0; i < args.length; i++) {
			ApplicationLogger.debug(APPLICATION_LOGGER, "ARGS[" + i + "] = " +  args[i]);
			ApplicationConstants.setValue("ARGS[" + i + "]", args[i]);
		}
		ApplicationLogger.debug(APPLICATION_LOGGER, "Arguments are set Successfully");
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "Exit");
	}

	private static int printError(int executionStatus) {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "Enter");
		String errorCode = ApplicationErrorHolder.getInstance().getError().getErrorCode();
		String errorMessage = ApplicationErrorHolder.getInstance().getError().getErrorMessage();
		if (executionStatus == 0)
			executionStatus = 1;
		ApplicationLogger.error(ERROR_LOGGER, "Error Code: " + errorCode + " Error Meaaage: " + errorMessage);
		System.err.print(errorCode + " : " + errorMessage);
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "Exit");
		return executionStatus;
	}
}
