package com.oracle.tools.upload.calendar.database.operations;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.oracle.tools.upload.calendar.constants.UploadUsers;
import com.oracle.tools.upload.calendar.context.ApplicationContextProvider;
import com.oracle.tools.upload.calendar.dataholders.NetworkCalKeyHolder;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

public class NetworkCalOperations {
	UploadUsers users;
	Connection connection;
	Set<String> networkCodeSet;
	String headOfficeBranchCode;
	Map<String, String> networkHostMap;

	public NetworkCalOperations(Connection connection) {
		this.connection = connection;
	}

	public boolean createYearData(Set<NetworkCalKeyHolder> keySet) {
		if(!fetchHOBranchCode())
			return false;
		if(!fetchNetworkHostMap())
			return false;
		// SELECT ho_branch FROM sttm_bank;
		users = (UploadUsers) ApplicationContextProvider.getContext().getBean("users");
		networkCodeSet = new HashSet<String>();
		try {
			keySet.forEach((keyData) -> {
				String networkCode = keyData.getNetworkCode();
				if (!networkCodeSet.contains(networkCode + keyData.getYear())) {
					networkCodeSet.add(networkCode + keyData.getYear());
					try (PreparedStatement statement = connection.prepareStatement(
							"SELECT COUNT(1) FROM PMTM_NETWORK_HOL_MASTER WHERE NETWORK_CODE=? and YEAR=?")) {
						statement.setString(1, networkCode);
						statement.setInt(2, keyData.getYear());
						try (ResultSet rs = statement.executeQuery()) {
							while (rs.next()) {
								int resultCount = rs.getInt(1);
								if (resultCount == 0)
									if (!createEntry(keyData.getYear(), keyData.getNetworkCode()))
										throw new Exception(
												"Failed in creating monthly data. createEntry returned false for Year="
														+ keyData.getYear() + " networkCode: " + networkCode);
							}
						}
					} catch (Exception exception) {
						ErrorHandler.logError("QUERY-ERROR-001",
								"Failed for year : " + keyData.getYear() + " networkCode: " + networkCode);
						ApplicationLogger.fatal(ERROR_LOGGER,
								"Failed for " + keyData.getYear() + " networkCode: " + networkCode, exception);
						throw new RuntimeException(exception);
					}
				}
			});
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private boolean fetchHOBranchCode() {
		try (PreparedStatement statement = connection.prepareStatement("SELECT ho_branch FROM sttm_bank")) {
			try (ResultSet rs = statement.executeQuery()) {
				while (rs.next()) {
					headOfficeBranchCode = rs.getString(1);
				}
			}
		} catch (Exception exception) {
			ErrorHandler.logError("QUERY-ERROR-006",
					"Failed: SELECT ho_branch FROM sttm_bank");
			ApplicationLogger.fatal(ERROR_LOGGER,
					"Failed:SELECT ho_branch FROM sttm_bank", exception);
			return false;
		}
		return true;
	}

	private boolean fetchNetworkHostMap() {
		networkHostMap = new HashMap<String, String>();
		try (Statement statement = connection.createStatement()) {
			try (ResultSet rs = statement.executeQuery(
					"SELECT network_code, host_code FROM PMTM_EXT_ACC_NETWORK_MAP where function_id='PMDACMAP'")) {
				while (rs.next()) {
					networkHostMap.put(rs.getString(1), rs.getString(2));
				}
			}
		} catch (Exception exception) {
			ErrorHandler.logError("QUERY-ERROR-005",
					"Failed for creating CountryCodeCurrencyMap table PMTM_EXT_ACC_NETWORK_MAP: ");
			ApplicationLogger.fatal(ERROR_LOGGER,
					"Failed for creating CountryCodeCurrencyMap table PMTM_EXT_ACC_NETWORK_MAP: ", exception);
			return false;
		}
		return true;
	}

	private boolean createEntry(Integer year, String networkCode) {
		String fullWorkingString = "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW";
		String query = "INSERT INTO PMTM_NETWORK_HOL_MASTER(NETWORK_CODE, COUNTRY_OFFICE_CODE ,YEAR, AUTH_STAT, MOD_NO, RECORD_STAT, ONCE_AUTH, MAKER_ID,CHECKER_ID, MAKER_DT_STAMP,CHECKER_DT_STAMP, WEEKLY_HOLIDAYS, HOST_CODE)"
				+ "VALUES(?,null,?,'A', 1,'O','Y',?,?,CURRENT_DATE,CURRENT_DATE , null,?)";
		String monthStringTemp = null;
		boolean insertedSuccessfully;
		int month = 0;
		try (PreparedStatement pmtmNetworkHolidayMaster = connection.prepareStatement(query)) {
			pmtmNetworkHolidayMaster.setString(1, networkCode);
			pmtmNetworkHolidayMaster.setInt(2, year);
			pmtmNetworkHolidayMaster.setString(3, users.getUser("MAKER_ID"));
			pmtmNetworkHolidayMaster.setString(4, users.getUser("CHECKER_ID"));
			pmtmNetworkHolidayMaster.setString(5, networkHostMap.get(networkCode));
			if (networkHostMap.get(networkCode) != null) {
				insertedSuccessfully = pmtmNetworkHolidayMaster.execute();
				for (month = 1; month <= 12; month++) {
					// Get the number of days in that month
					YearMonth yearMonthObject = YearMonth.of(year, month);
					int daysInMonth = yearMonthObject.lengthOfMonth();
					StringBuffer monthString = new StringBuffer(fullWorkingString.substring(0, daysInMonth));
	
					List<LocalDate> weekendList = getWeekends(year, month);
					weekendList.forEach(satSun -> {
						int dayOfWeek = satSun.getDayOfMonth();
						monthString.setCharAt(dayOfWeek - 1, 'H');
					});
					monthStringTemp = monthString.toString();
	
					try (PreparedStatement pmtmNetworkHolidayInsert = connection.prepareStatement(
							"INSERT INTO PMTM_NETWORK_HOLIDAY(NETWORK_CODE, COUNTRY_OFFICE_CODE, YEAR, MONTH, HOLIDAY_LIST, HOST_CODE)"
									+ "VALUES(?,?,?,?,?,?)")) {
	
						pmtmNetworkHolidayInsert.setString(1, networkCode);
						pmtmNetworkHolidayInsert.setString(2, headOfficeBranchCode);
						pmtmNetworkHolidayInsert.setInt(3, year);
						pmtmNetworkHolidayInsert.setInt(4, month);
						pmtmNetworkHolidayInsert.setString(5, monthString.toString());
						pmtmNetworkHolidayInsert.setString(6, networkHostMap.get(networkCode));
						insertedSuccessfully = pmtmNetworkHolidayInsert.execute();
					}
				}
			}else {
				ErrorHandler.logError("PARSE-ERROR-001", "Invalid Network Code in Upload file - Failed");
			}

		} catch (Exception e) {
			ErrorHandler.logError("QUERY-ERROR-002", "INSERT INTO PMTM_NETWORK_HOLIDAY Failed");
			ApplicationLogger.error(ERROR_LOGGER, "INSERT INTO PMTM_NETWORK_HOLIDAY(YEAR, MONTH, HOLIDAY_LIST)");
			ApplicationLogger.error(ERROR_LOGGER, "year: " + year);
			ApplicationLogger.error(ERROR_LOGGER, "month: " + month);
			ApplicationLogger.error(ERROR_LOGGER, "monthString: " + monthStringTemp);
			ApplicationLogger.error(ERROR_LOGGER, "networkCode: " + networkCode);
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in STTM_LCL_HOLIDAY INSERT", e);
			return false;
		}
		return true;
	}

	private List<LocalDate> getWeekends(int year, int month) {
		LocalDate firstDateOfTheMonth = YearMonth.of(year, month).atDay(1);
		List<LocalDate> list = new ArrayList<>();

		for (LocalDate date = firstDateOfTheMonth; !date
				.isAfter(firstDateOfTheMonth.with(TemporalAdjusters.lastDayOfMonth())); date = date.plusDays(1))
			if (date.getDayOfWeek() == DayOfWeek.SATURDAY || date.getDayOfWeek() == DayOfWeek.SUNDAY)
				list.add(date);

		return list;
	}

	public Map<NetworkCalKeyHolder, String> getMonthlyData(NetworkCalKeyHolder keyData) {
		Map<NetworkCalKeyHolder, String> yearDataDB = new HashMap<NetworkCalKeyHolder, String>();
		String query = "SELECT month, holiday_list FROM PMTM_NETWORK_HOLIDAY WHERE network_code=? AND year=?";
		try (PreparedStatement sttmLclHolidaySelect = connection.prepareStatement(query)) {
			sttmLclHolidaySelect.setString(1, keyData.getNetworkCode());
			sttmLclHolidaySelect.setInt(2, keyData.getYear());
			try (ResultSet rs = sttmLclHolidaySelect.executeQuery()) {
				while (rs.next()) {
					int month = rs.getInt(1);
					String holidayString = rs.getString(2);
					yearDataDB.put(new NetworkCalKeyHolder(keyData.getYear(), keyData.getNetworkCode(), month),
							holidayString);
				}
			}
		} catch (SQLException e) {
			ErrorHandler.logError("QUERY-ERROR-003", "Select from PMTM_NETWORK_HOLIDAY Failed");
			ApplicationLogger.error(ERROR_LOGGER, "SELECT month, holiday_list FROM PMTM_NETWORK_HOLIDAY WHERE year="
					+ keyData.getYear() + " Currency: " + keyData.getNetworkCode());
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in PMTM_NETWORK_HOLIDAY", e);
		}
		return yearDataDB;
	}

	public void uploadNetworkHolidays(Map<NetworkCalKeyHolder, String> updatedData) {
		String query = "UPDATE PMTM_NETWORK_HOLIDAY SET HOLIDAY_LIST = ? WHERE NETWORK_CODE=? AND YEAR = ? AND MONTH = ?";
		updatedData.forEach((keyData, holidayString) -> {
			try (PreparedStatement pmtmNetworkHoliday = connection.prepareStatement(query)) {
				pmtmNetworkHoliday.setString(1, holidayString);
				pmtmNetworkHoliday.setString(2, keyData.getNetworkCode());
				pmtmNetworkHoliday.setInt(3, keyData.getYear());
				pmtmNetworkHoliday.setInt(4, keyData.getMonth());
				pmtmNetworkHoliday.executeUpdate();
			}

			catch (SQLException e) {
				ErrorHandler.logError("QUERY-ERROR-004", "UPDATE PMTM_NETWORK_HOLIDAY Failed");
				ApplicationLogger.error(ERROR_LOGGER,
						"UPDATE STTM_CCY_HOLIDAY SET HOLIDAY_LIST =" + holidayString + "WHERE YEAR = "
								+ keyData.getYear() + " AND MONTH = " + keyData.getMonth() + "Network: "
								+ keyData.getNetworkCode());
				ApplicationLogger.fatal(ERROR_LOGGER, "Failed in PMTM_NETWORK_HOLIDAY UPDATE", e);
			}
		});
	}
}
