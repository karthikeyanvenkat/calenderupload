package com.oracle.tools.upload.calendar.uploaders;

public interface IUploader {
	public boolean upload();
}
