package com.oracle.tools.upload.calendar.tasks.argsvalidator;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.*;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Setter;

@Setter
public class ArgsLengthValidator extends AbstractTask {
	private String invalidArgsLengthErrorCode;
	
	@Override
	protected boolean process() {
		boolean controllerReturnValue = true;
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "Enter");
		int argsSize = Integer.parseInt(ApplicationConstants.getValue("ARGS_LENGTH")) -1;
		int validatorsSize = getTaskList().size();
		controllerReturnValue = validateLength(argsSize, validatorsSize);
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "Exit");
		return controllerReturnValue;
	}

	private boolean validateLength(int argsLength, int listSize) {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "Enter");
		boolean validationResult = true;
		ApplicationLogger.info(APPLICATION_LOGGER, "Validating agrs Length against expected length");
		if (argsLength == listSize) {
			validationResult = true;
		} else {
			ApplicationLogger.error(APPLICATION_LOGGER, "Invalid Count of Arguments Input to Java Program");
			ApplicationLogger.error(ERROR_LOGGER, "Failed in Validating agrs Length: " + argsLength
					+ " against expected length: " + listSize
					+ " \n Please check in Bean.xml and correct the values as per  expected number of input Parameters");
			ErrorHandler.logError(invalidArgsLengthErrorCode);
			validationResult = false;
		}
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "Exit");
		return validationResult;
	}

}
