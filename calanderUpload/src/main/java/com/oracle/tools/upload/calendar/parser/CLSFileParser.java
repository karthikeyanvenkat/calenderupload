package com.oracle.tools.upload.calendar.parser;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.dataholders.CLSCalDataHolder;
import com.oracle.tools.upload.calendar.dataholders.CLSCalKeyHolder;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

import lombok.Getter;
import lombok.Setter;

public class CLSFileParser {
	@Setter
	private String invalidFileData;
	private volatile String errorData;
	@Getter
	Map<CLSCalKeyHolder, List<CLSCalDataHolder>> parsedData;
	private List<String> invalidData;
	private String partUploadFailed = "PART_WRONG_FORMAT";

	public boolean parseFile() {
		parsedData = new HashMap<CLSCalKeyHolder, List<CLSCalDataHolder>>();
		String fileName = ApplicationConstants.getValue("FILE_NAME");
		String directory = ApplicationConstants.getValue("PREPROCESS_DIR");
		String filePath = directory + File.separator + fileName;
		File f = new File(filePath);
		Path file = f.toPath();
		Charset charset = Charset.defaultCharset();
		if (f.exists()) {
			try {
				Files.lines(file, charset).forEach(line -> {
					if (line != null && !line.isEmpty() && (line.charAt(8) == 'W' || line.charAt(8) == 'H')) {
						try {
							CLSCalKeyHolder keyDataHolder;
							int year = 0;
							CLSCalDataHolder lineData = new CLSCalDataHolder();
							year = Integer.parseInt(line.substring(0, 4));
							keyDataHolder = new CLSCalKeyHolder(year);
							lineData.setMonth(Integer.parseInt(line.substring(4, 6)));
							lineData.setDate(Integer.parseInt(line.substring(6, 8).trim()));
							lineData.setHolidayType(line.charAt(8));
							if (parsedData.get(keyDataHolder) == null) {
								parsedData.put(keyDataHolder, new ArrayList<CLSCalDataHolder>());
							}
							parsedData.get(keyDataHolder).add(lineData);
							
						} catch (Exception e) {
							errorData = line;
							if(invalidData == null)
								invalidData = new ArrayList<String>();
							invalidData.add(line);
							//throw e; /*Commented this line to process valid lines in Upload file */
						}
					}else if(!(line.charAt(8) == 'W' || line.charAt(8) == 'H')) {
						if(invalidData == null)
							invalidData = new ArrayList<String>();
						invalidData.add(line);
					}
					
				});
			} catch (Exception e) {
				ApplicationLogger.fatal(ERROR_LOGGER, "Failed while parsing data: " + errorData, e);
				ErrorHandler.logError(invalidFileData);
				//return false; /*Commented this line to process valid lines in Upload file */
			}
		}
		if(invalidData != null) {
			ErrorHandler.logError(this.partUploadFailed);
		}
		return true;
	}
}
