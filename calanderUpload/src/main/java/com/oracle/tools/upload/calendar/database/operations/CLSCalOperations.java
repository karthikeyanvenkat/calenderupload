package com.oracle.tools.upload.calendar.database.operations;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.YearMonth;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.oracle.tools.upload.calendar.constants.UploadUsers;
import com.oracle.tools.upload.calendar.context.ApplicationContextProvider;
import com.oracle.tools.upload.calendar.dataholders.CLSCalKeyHolder;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

public class CLSCalOperations {

	private UploadUsers users;
	private Connection connection;

	public CLSCalOperations(Connection connection) {
		this.connection = connection;
	}

	public boolean createYearData(Set<CLSCalKeyHolder> keySet) {
		users = (UploadUsers) ApplicationContextProvider.getContext().getBean("users");
		try {
			keySet.forEach((keyData) -> {
				try (PreparedStatement statement = connection
						.prepareStatement("SELECT COUNT(1) FROM FSTM_CLS_HOL_MASTER WHERE YEAR=?")) {
					statement.setInt(1, keyData.getYear());
					try (ResultSet rs = statement.executeQuery()) {
						while (rs.next()) {
							int resultCount = rs.getInt(1);
							if (resultCount == 0)
								if (!createEntry(keyData.getYear()))
									throw new Exception(
											"Failed in creating monthly data. createEntry returned false for Year="
													+ keyData.getYear());
						}
					}
				} catch (Exception exception) {
					ErrorHandler.logError("QUERY-ERROR-001", "Failed for year : " + keyData.getYear());
					ApplicationLogger.fatal(ERROR_LOGGER, "Failed for " + keyData.getYear(), exception);
					throw new RuntimeException(exception);
				}
			});
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	private boolean createEntry(int year) {
		Calendar calendar = Calendar.getInstance();
		String fullWorkingString = "WWWWWWWWWWWWWWWWWWWWWWWWWWWWWWW";
		int month = 0;
		StringBuffer monthString = null;
		String query = "INSERT INTO FSTM_CLS_HOL_MASTER(AUTH_STAT, YEAR, MOD_NO, RECORD_STAT, WEEKLY_HOLIDAYS ,CHECKER_DT_STAMP,CHECKER_ID, MAKER_DT_STAMP,MAKER_ID, ONCE_AUTH, UNEXP_HOL)"
				+ "VALUES('A',?,1,'O',null, CURRENT_DATE , ?,CURRENT_DATE,?,'Y',null )";
		try (PreparedStatement fstmClsDolMasterInsert = connection.prepareStatement(query)) {
			fstmClsDolMasterInsert.setInt(1, year);
			fstmClsDolMasterInsert.setString(2, users.getUser("CHECKER_ID"));
			fstmClsDolMasterInsert.setString(3, users.getUser("MAKER_ID"));
			boolean insertedSuccessfully = fstmClsDolMasterInsert.execute();
			for (month = 1; month <= 12; month++) {
				// Get the number of days in that month
				YearMonth yearMonthObject = YearMonth.of(year, month);
				int daysInMonth = yearMonthObject.lengthOfMonth();
				monthString = new StringBuffer(fullWorkingString.substring(0, daysInMonth));
				for (int day = 1; day <= daysInMonth; day++) {
					calendar.set(year, month - 1, day);
					int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
					if (dayOfWeek == Calendar.SUNDAY || dayOfWeek == Calendar.SATURDAY) {
						monthString.setCharAt(day - 1, 'H');
					}
				}

				try (PreparedStatement fstmLClsHolidayInsert = connection.prepareStatement(
						"INSERT INTO FSTM_CLS_HOLIDAY(YEAR, MONTH, HOLIDAY_LIST)" + "VALUES(?,?,?)")) {
					fstmLClsHolidayInsert.setInt(1, year);
					fstmLClsHolidayInsert.setInt(2, month);
					fstmLClsHolidayInsert.setString(3, monthString.toString());
					insertedSuccessfully = fstmLClsHolidayInsert.execute();
				}
			}

		} catch (Exception e) {
			ErrorHandler.logError("QUERY-ERROR-002", "INSERT INTO FSTM_CLS_HOLIDAY Failed");
			ApplicationLogger.error(ERROR_LOGGER, "INSERT INTO FSTM_CLS_HOLIDAY(YEAR, MONTH, HOLIDAY_LIST)");
			ApplicationLogger.error(ERROR_LOGGER, "year: " + year);
			ApplicationLogger.error(ERROR_LOGGER, "month: " + month);
			ApplicationLogger.error(ERROR_LOGGER, "monthString: " + monthString);
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in STTM_LCL_HOLIDAY INSERT", e);
			return false;
		}
		return true;
	}

	public Map<CLSCalKeyHolder, String> getMonthlyData(CLSCalKeyHolder keyData) {
		Map<CLSCalKeyHolder, String> yearDataDB = new HashMap<CLSCalKeyHolder, String>();
		String query = "SELECT month, holiday_list FROM FSTM_CLS_HOLIDAY WHERE year=?";
		try (PreparedStatement fstmClsHolidaySelect = connection.prepareStatement(query)) {
			fstmClsHolidaySelect.setInt(1, keyData.getYear());
			try (ResultSet rs = fstmClsHolidaySelect.executeQuery()) {
				while (rs.next()) {
					int month = rs.getInt(1);
					String holidayString = rs.getString(2);
					yearDataDB.put(new CLSCalKeyHolder(keyData.getYear(), month), holidayString);
				}
			}
		} catch (SQLException e) {
			ErrorHandler.logError("QUERY-ERROR-003", "Select from FSTM_CLS_HOLIDAY Failed");
			ApplicationLogger.error(ERROR_LOGGER,
					"SELECT month, holiday_list FROM FSTM_CLS_HOLIDAY WHERE year=" + keyData.getYear());
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in FSTM_CLS_HOLIDAY", e);
		}
		return yearDataDB;
	}

	public void uploadCLSHolidays(Map<CLSCalKeyHolder, String> updatedData) {
		String query = "UPDATE FSTM_CLS_HOLIDAY SET HOLIDAY_LIST = ? WHERE YEAR = ? AND MONTH = ?";
		updatedData.forEach((keyData, holidayString) -> {
			try (PreparedStatement sttmLclHolidayUpdate = connection.prepareStatement(query)) {
				sttmLclHolidayUpdate.setString(1, holidayString);
				sttmLclHolidayUpdate.setInt(2, keyData.getYear());
				sttmLclHolidayUpdate.setInt(3, keyData.getMonth());
				sttmLclHolidayUpdate.executeUpdate();
			}

			catch (SQLException e) {
				ErrorHandler.logError("QUERY-ERROR-004", "UPDATE FSTM_CLS_HOLIDAY Failed");
				ApplicationLogger.error(ERROR_LOGGER, "UPDATE FSTM_CLS_HOLIDAY SET HOLIDAY_LIST =" + holidayString
						+ "WHERE YEAR = " + keyData.getYear() + " AND MONTH = " + keyData.getMonth());
				ApplicationLogger.fatal(ERROR_LOGGER, "Failed in FSTM_CLS_HOLIDAY UPDATE", e);
			}
		});
	}
}
