package com.oracle.tools.upload.calendar.connection.database;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.security.Security;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

import lombok.Setter;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

public class WalletConnection implements IConnectionProvider {

	@Setter
	private String tnsAlias, walletPath, tnsPath;
	@Setter
	private Integer minPoolSize, maxPoolSize, initialPoolSize;

	private PoolDataSource pds;

	private synchronized void createPooledDatasource() {
		Security.insertProviderAt(new oracle.security.pki.OraclePKIProvider(), 3);
		System.setProperty(ApplicationConstants.WALLET_LOCATION_PROPERTY, walletPath);
		System.setProperty(ApplicationConstants.TNS_ADMIN_PROPERTY, tnsPath);

		Properties prop = new Properties();
		prop.setProperty("validateconnection", "true");

		try {
			pds = PoolDataSourceFactory.getPoolDataSource();
			pds.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
			pds.setURL(ApplicationConstants.ORACLE_URL_PROPERTY + tnsAlias);
			pds.setConnectionProperties(prop);
			pds.setInitialPoolSize(initialPoolSize);
			pds.setMinPoolSize(minPoolSize);
			pds.setMaxPoolSize(maxPoolSize);
			// pds.setValidateConnectionOnBorrow(true);
			// pds.setSQLForValidateConnection("select 1 from DUAL");
		} catch (Exception exception) {
			ErrorHandler.logError("DB-ERROR-001","Failed in getting connection from pooled Datasource");
			ApplicationLogger.error(ERROR_LOGGER, "tnsAlias: " + tnsAlias);
			ApplicationLogger.error(ERROR_LOGGER, "walletPath: " + walletPath);
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in creating pooled Datasource" , exception);
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in getting connection from pooled Datasource" , exception);
		}
	}

	@Override
	public Connection getConnection() throws Exception {
		Connection connection = null;
		if (pds == null)
			createPooledDatasource();
		try {
			connection = pds.getConnection();
		} catch (SQLException exception) {
			ErrorHandler.logError("DB-ERROR-002","Failed in creating pooled Datasource");
			ApplicationLogger.fatal(ERROR_LOGGER, "Failed in creating pooled Datasource" , exception);
		}
		return connection;
	}

}
