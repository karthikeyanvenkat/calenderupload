package com.oracle.tools.upload.calendar.tasks;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.nio.file.Path;
import java.nio.file.Paths;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Setter;

@Setter
public class SplitFile extends AbstractTask {

	private String sourceRef, errorFileSplitFailed;
	private FileSplitResolver splitterFinder;

	@Override
	protected boolean process() {
		boolean fileSplitResult = true;
		try {
			Path file = Paths.get(ApplicationConstants.getValue(sourceRef));
			splitterFinder.getFileSplitter().splitFile(file);
		} catch (Exception exception) {
			fileSplitResult = false;
			ApplicationLogger.fatal(ERROR_LOGGER, "File Split Failed", exception);
			ErrorHandler.logError(errorFileSplitFailed);
		}
		return fileSplitResult;
	}

}
