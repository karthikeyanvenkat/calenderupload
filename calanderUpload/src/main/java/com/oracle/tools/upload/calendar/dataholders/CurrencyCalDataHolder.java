package com.oracle.tools.upload.calendar.dataholders;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class CurrencyCalDataHolder {

	private int date, month;
	private char holidayType, modificationType;
	
}
