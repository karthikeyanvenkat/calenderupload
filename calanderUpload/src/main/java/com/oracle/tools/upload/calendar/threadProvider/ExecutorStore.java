package com.oracle.tools.upload.calendar.threadProvider;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import lombok.Getter;

public class ExecutorStore {
	@Getter
	private ExecutorService executorService;
	
	public ExecutorStore(int numberOfThreads)
	{
		executorService = Executors.newFixedThreadPool(numberOfThreads);
	}
}
