package com.oracle.tools.upload.calendar.error;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.*;

import java.util.Map;

import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

import lombok.Setter;

public class ErrorCodesHolder {

	@Setter
	private Map<String, Error> allErrors;

	public Error getExternalError(String errorCode) {
		Error error = null;
		if (allErrors != null && allErrors.containsKey(errorCode))
			error = allErrors.get(errorCode);
		else if (allErrors == null)
			ApplicationLogger.fatal(ERROR_LOGGER,
					"Invalid Beans.xml configuration for ErrorCodesHolder allErrors is null");
		else if (!allErrors.containsKey(errorCode))
			ApplicationLogger.fatal(ERROR_LOGGER,
					"Invalid Beans.xml configuration for ErrorCodesHolder allErrors does not contain configurstion for "
							+ errorCode);
		else
			ApplicationLogger.fatal(ERROR_LOGGER,
					"Unexpected error Invalid Beans.xml configuration for ErrorCodesHolder for "
							+ errorCode);
		if (error == null) {
			error = new Error();
			error.setErrorCode("999");
			error.setErrorMessage("Fatal Error");
		}
		return error;
	}
}
