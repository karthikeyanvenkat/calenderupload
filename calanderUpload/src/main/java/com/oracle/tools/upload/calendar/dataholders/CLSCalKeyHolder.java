package com.oracle.tools.upload.calendar.dataholders;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CLSCalKeyHolder {
	private int year;
	private int month = 0;

	public CLSCalKeyHolder(int year) {
		this.year = year;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + month;
		result = prime * result + year;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CLSCalKeyHolder other = (CLSCalKeyHolder) obj;
		if (month != other.month)
			return false;
		if (year != other.year)
			return false;
		return true;
	}

}
