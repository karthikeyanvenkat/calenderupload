package com.oracle.tools.upload.calendar.tasks.upload;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oracle.tools.upload.calendar.connection.database.IConnectionProvider;
import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.database.operations.NetworkCalOperations;
import com.oracle.tools.upload.calendar.dataholders.NetworkCalDataHolder;
import com.oracle.tools.upload.calendar.dataholders.NetworkCalKeyHolder;
import com.oracle.tools.upload.calendar.parser.NetworkFileParser;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Setter;

public class NetworkCalUpload extends AbstractTask {
	@Setter
	IConnectionProvider connectionProvider;
	@Setter
	String invalidFileData;
	NetworkCalOperations networkCalOperations;

	@Override
	protected boolean process() {
		boolean processResult = true;
		if (ApplicationConstants.getValue("ARGS[3]").equals("NetworkCalendar")) {
			Map<NetworkCalKeyHolder, List<NetworkCalDataHolder>> fileData = null;
			NetworkFileParser fileParser = new NetworkFileParser();
			Connection connection = null;
			try {
				connection = connectionProvider.getConnection();
			} catch (Exception e) {
				processResult = false;
			}
			if (processResult) {
				networkCalOperations = new NetworkCalOperations(connection);
				fileParser.setInvalidFileData(invalidFileData);
				processResult = fileParser.parseFile();
				if (processResult)
					fileData = fileParser.getParsedData();
				if (!fileData.isEmpty()) {
					processResult = networkCalOperations.createYearData(fileData.keySet());
					upload(fileData);
				}
			}
		}

		return processResult;
	}

	private void upload(Map<NetworkCalKeyHolder, List<NetworkCalDataHolder>> fileData) {
		Map<NetworkCalKeyHolder, String> updatedData = new HashMap<NetworkCalKeyHolder, String>();
		fileData.forEach((keyData, dataList) -> {
			Map<NetworkCalKeyHolder, String> yearDataDB = networkCalOperations.getMonthlyData(keyData);
			dataList.forEach((data) -> {
				StringBuffer monthString;
				NetworkCalKeyHolder branchCalKeyHolder = new NetworkCalKeyHolder(keyData.getYear(),
						keyData.getNetworkCode(), data.getMonth());
				if (updatedData.get(branchCalKeyHolder) == null) {
					updatedData.put(branchCalKeyHolder, yearDataDB.get(branchCalKeyHolder));
				}
				
				if (updatedData.get(branchCalKeyHolder) != null) {
					monthString = new StringBuffer(updatedData.get(branchCalKeyHolder));
					monthString.setCharAt(data.getDate() - 1, data.getHolidayType());
					updatedData.put(branchCalKeyHolder, monthString.toString());
				}
			});
		});
		networkCalOperations.uploadNetworkHolidays(updatedData);
	}

}
