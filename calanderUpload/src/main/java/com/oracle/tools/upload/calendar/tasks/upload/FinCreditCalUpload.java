package com.oracle.tools.upload.calendar.tasks.upload;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oracle.tools.upload.calendar.connection.database.IConnectionProvider;
import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.database.operations.FinCreditCalOperations;
import com.oracle.tools.upload.calendar.dataholders.FinancialCalDataHolder;
import com.oracle.tools.upload.calendar.dataholders.FinancialCalKeyHolder;
import com.oracle.tools.upload.calendar.parser.FinancialFileParser;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Setter;

public class FinCreditCalUpload extends AbstractTask {
	@Setter
	IConnectionProvider connectionProvider;
	@Setter
	String invalidFileData;
	FinCreditCalOperations clearingHouseOperations;

	@Override
	protected boolean process() {
		boolean processResult = true;
		if (ApplicationConstants.getValue("ARGS[3]").equals("FinancialCalCredit")) {
			Map<FinancialCalKeyHolder, List<FinancialCalDataHolder>> fileData = null;
			FinancialFileParser fileParser = new FinancialFileParser();
			Connection connection = null;
			try {
				connection = connectionProvider.getConnection();
			} catch (Exception e) {
				processResult = false;
			}
			if (processResult) {
				clearingHouseOperations = new FinCreditCalOperations(connection);
				fileParser.setInvalidFileData(invalidFileData);
				processResult = fileParser.parseFile();
				if (processResult)
					fileData = fileParser.getParsedData();
				if (!fileData.isEmpty()) {
					processResult = clearingHouseOperations.createYearData(fileData.keySet());
					upload(fileData);
				}
			}
		}
		return processResult;
	}

	private void upload(Map<FinancialCalKeyHolder, List<FinancialCalDataHolder>> fileData) {
		Map<FinancialCalKeyHolder, String> updatedData = new HashMap<FinancialCalKeyHolder, String>();
		fileData.forEach((keyData, dataList) -> {
			Map<FinancialCalKeyHolder, String> yearDataDB = clearingHouseOperations.getMonthlyData(keyData);
			dataList.forEach((data) -> {
				StringBuffer monthString;
				FinancialCalKeyHolder clearingHouseCalKeyHolder = new FinancialCalKeyHolder(keyData.getYear(),
						keyData.getClearingHouseCode(), data.getMonth());
				if (updatedData.get(clearingHouseCalKeyHolder) == null) {
					updatedData.put(clearingHouseCalKeyHolder, yearDataDB.get(clearingHouseCalKeyHolder));
				}
				monthString = new StringBuffer(updatedData.get(clearingHouseCalKeyHolder));
				monthString.setCharAt(data.getDate() - 1, data.getHolidayType());
				updatedData.put(clearingHouseCalKeyHolder, monthString.toString());
			});
		});
		clearingHouseOperations.uploadClearingHouseHolidays(updatedData);
	}
}
