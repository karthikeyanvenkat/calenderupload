package com.oracle.tools.upload.calendar.tasks.argsvalidator;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.io.File;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import com.oracle.tools.upload.calendar.logging.LoggingFileType;

public class ArgBeansFileValidator {

	public int process() {
		ApplicationLogger.trace(LoggingFileType.PERFORMANCE_LOGGER, "Enter");
		int fileValidation = 0;
		int argsLength = Integer.parseInt(ApplicationConstants.getValue("ARGS_LENGTH"));
		if (argsLength > 0) {
			String filePath = ApplicationConstants.getValue("ARGS[0]");
			File f = new File(filePath);
			if (f.exists() && f.isFile() && f.canRead()) {
				ApplicationLogger.trace(LoggingFileType.APPLICATION_LOGGER,	"File " + filePath + " basic validation success");
			} else if (!f.exists()) {
				ErrorHandler.logError("FATAL-002", "Beans.xml file missing");
				ApplicationLogger.error(LoggingFileType.ERROR_LOGGER, "File " + filePath + " does not exist");
				fileValidation = 2;
			} else if (!f.isFile()) {
				ApplicationLogger.error(LoggingFileType.ERROR_LOGGER, "File " + filePath + " is not a file");
				ErrorHandler.logError("FATAL-003", "Beans.xmlfile provided path is not a file");
				fileValidation = 3;
			} else if (!f.canRead()) {
				ApplicationLogger.error(LoggingFileType.ERROR_LOGGER, "File " + filePath + " is not readable");
				ErrorHandler.logError("FATAL-004", "Beans.xml can't be read");
				fileValidation = 4;
			}
		} else {
			ApplicationLogger.error(ERROR_LOGGER, "Fist argument of Beans.xml full path to Java program is manatory");
			ErrorHandler.logError("FATAL-001", "Fist argument of Beans.xml full path is missing");
			fileValidation = 9;
		}

		ApplicationLogger.trace(LoggingFileType.PERFORMANCE_LOGGER, "Exit");
		return fileValidation;
	}
}
