package com.oracle.tools.upload.calendar.FileSplitter;

import java.nio.file.Path;

public interface IFileSplitter {
	public boolean splitFile(Path file);
}
