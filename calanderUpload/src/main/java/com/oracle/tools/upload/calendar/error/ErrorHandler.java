package com.oracle.tools.upload.calendar.error;

import com.oracle.tools.upload.calendar.context.ApplicationContextProvider;

public class ErrorHandler {

	public static void logError(String errorCode) {
		if (ApplicationContextProvider.getContext() != null) {
			ErrorCodesHolder errorCodesHolder = (ErrorCodesHolder) ApplicationContextProvider.getContext()
					.getBean("errorCodesHolder");
			ApplicationErrorHolder.getInstance().setError(errorCodesHolder.getExternalError(errorCode));
		}
	}

	public static void logError(String errorCode, String errorMessage) {
		Error error = new Error();
		error.setErrorCode(errorCode);
		error.setErrorMessage(errorMessage);
		ApplicationErrorHolder.getInstance().setError(error);
	}
	
	public static void logError(String errorCode, Exception exception) {
		Error error = new Error();
		error.setErrorCode(errorCode);
		error.setException(exception);
		ApplicationErrorHolder.getInstance().setError(error);
	}
}
