package com.oracle.tools.upload.calendar.FileSplitter;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.oracle.tools.upload.calendar.connection.database.IConnectionProvider;
import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.database.operations.BranchCalOperations;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;

import lombok.Setter;

@Setter
public class BranchCalendarFileSplitter implements IFileSplitter {

	private String preProcessDirRef;
	private List<IConnectionProvider> allPDBConnections;
	private List<String> branchFullList;
	private String errorUploadFailed = "BRN_WRONG_CODE";
    private String partUploadFailed = "PART_BRN_UPLOAD";
    

	@Override
	public boolean splitFile(Path file) {
		BranchCalOperations branchCalOperations;
		try {
			branchCalOperations = new BranchCalOperations(allPDBConnections.get(0).getConnection());
		} catch (Exception e) {
			return false;
		}
		if (!branchCalOperations.validatePDBCount(allPDBConnections.size()))
			return false;
		Map<String, String> identifierToEntity = createIdentifierToEntityMap();
		if (identifierToEntity == null || identifierToEntity.size() == 0)
			return false;
		Boolean[] streamResult = new Boolean[1];
		streamResult[0] = true;
		boolean fileSplitResult;
		String fileName = file.getFileName().toString();
		ApplicationConstants.setValue("FILE_NAME", fileName);
		identifierToEntity.forEach((key, value) -> {
			try (Stream<String> stream = Files.lines(file)) {
				/*
				 * ArrayList<String> fileData = (ArrayList<String>) stream.filter(line ->
				 * line.startsWith(key)) .map(String::toUpperCase).collect(Collectors.toList());
				 */
				ArrayList<String> fileData = (ArrayList<String>) stream.filter(line -> line.startsWith(key) && ( line.charAt(11) == 'W'|| line.charAt(11) == 'H'))
						.collect(Collectors.toList());
				if (fileData != null && fileData.size() > 0) {
					FileWriter writer = null;
					String newFilePath = ApplicationConstants.getValue(preProcessDirRef) + File.separator + value
							+ fileName;
					File fileFullPath = new File(newFilePath);
					if (!fileFullPath.exists()) {
						writer = new FileWriter(newFilePath);
					} else {
						writer = new FileWriter(newFilePath, true);
					}
					for (String str : fileData) {
						writer.write(str + System.lineSeparator());
					}
					writer.close();
				}
				
				
			} catch (IOException e) {
				ApplicationLogger.fatal(ERROR_LOGGER, "File Split Failed", e);
				streamResult[0] = false;
			}
		});
		fileSplitResult = streamResult[0];
		validateFile(file);
		return fileSplitResult;
	}
	
	private boolean validateFile(Path file) {
        boolean result = true;
        try (Stream<String> stream = Files.lines(file)) {
            String validIdentifiers = branchFullList.stream().collect(Collectors.joining("|"));
//              List<String> invalidData = stream.filter(line -> line.matches("^(?!LDN|LHO|LDB|GAS|TKY).*$")).collect(Collectors.toList());
                List<String> invalidData = stream.filter(line -> line.matches("^(?!"+validIdentifiers+").*$") || !(line.charAt(11) == 'W' || line.charAt(11) == 'H')).collect(Collectors.toList());
                try (Stream<String> stream1 = Files.lines(file)) {
	                List<String> validData = stream1.filter(line -> line.matches("^("+validIdentifiers+").*$") && (line.charAt(11) == 'W' || line.charAt(11) == 'H')).collect(Collectors.toList());
	                if (invalidData != null && invalidData.size() > 0) {
	                	if(validData != null && validData.size() > 0) {
		                    ApplicationLogger.error(ERROR_LOGGER, "File contains wrong Format" + invalidData);
		                    result = false;
		                    ErrorHandler.logError(this.partUploadFailed);
		                    
	                	}else {
	                		ApplicationLogger.error(ERROR_LOGGER, "File contains wrong Format" + invalidData);
		                    result = false;
		                    ErrorHandler.logError(this.errorUploadFailed);
	                	}
	                }
                } catch (IOException e) {
                    ApplicationLogger.fatal(ERROR_LOGGER, "File Split Failed", e);
                    result = false;
                } 
                
        } catch (IOException e) {
            ApplicationLogger.fatal(ERROR_LOGGER, "File Split Failed", e);
            result = false;
        }
        return result;
    }

	private Map<String, String> createIdentifierToEntityMap() {
		Map<String, String> identifierToEntityMap = new HashMap<String, String>();
		for (IConnectionProvider PDBConnection : allPDBConnections) {
			try {
				BranchCalOperations branchCalOperations = new BranchCalOperations(PDBConnection.getConnection());
				String headOffice = branchCalOperations.getHO();
				List<String> branchList = branchCalOperations.getBranches();
				if(branchFullList == null)
					branchFullList = new ArrayList<String>();
				branchFullList.addAll(branchList);
				branchList.forEach(item -> identifierToEntityMap.put(item, headOffice));
			} catch (Exception e) {
				return null;
			}
		}
		return identifierToEntityMap;
	}
}
