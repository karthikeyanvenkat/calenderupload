package com.oracle.tools.upload.calendar.constants;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class EntitiesList {
	private List<String> entitiesList;
}
