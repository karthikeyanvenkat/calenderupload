package com.oracle.tools.upload.calendar.error;

import lombok.Getter;
import lombok.Setter;

public class ApplicationErrorHolder {
	@Getter @Setter
	private Error error;

	private static ApplicationErrorHolder instance;

	private ApplicationErrorHolder() {
	}

	public static ApplicationErrorHolder getInstance() {
		if (instance == null)
			instance = new ApplicationErrorHolder();
		return instance;
	}
}
