package com.oracle.tools.upload.calendar.tasks.upload;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oracle.tools.upload.calendar.connection.database.IConnectionProvider;
import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.database.operations.BranchCalOperations;
import com.oracle.tools.upload.calendar.dataholders.BranchCalDataHolder;
import com.oracle.tools.upload.calendar.dataholders.BranchCalKeyHolder;
import com.oracle.tools.upload.calendar.parser.BranchFileParser;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Setter;

public class BranchCalUpload extends AbstractTask {
	@Setter
	IConnectionProvider connectionProvider;
	@Setter
	String invalidFileData;

	BranchCalOperations branchOperations;

	@Override
	protected boolean process() {
		String fileNameAppender = null;
		boolean processResult = true;
		if (ApplicationConstants.getValue("ARGS[3]").equals("BranchCalendar")) {
			Map<BranchCalKeyHolder, List<BranchCalDataHolder>> fileData = null;
			BranchFileParser fileParser = new BranchFileParser();
			Connection connection = null;
			try {
				connection = connectionProvider.getConnection();
				branchOperations = new BranchCalOperations(connection);
				fileNameAppender = branchOperations.getHO();
			} catch (Exception e) {
				processResult = false;
			}
			if (processResult) {
				fileParser.setInvalidFileData(invalidFileData);
				processResult = fileParser.parseFile(fileNameAppender);
				if (processResult)
					fileData = fileParser.getParsedData();
				if (!fileData.isEmpty()) {
					processResult = branchOperations.createYearData(fileData.keySet());
					processResult = upload(fileData);
				}
			}
		}
		return processResult;
	}

	private boolean upload(Map<BranchCalKeyHolder, List<BranchCalDataHolder>> fileData) {
		Map<BranchCalKeyHolder, String> updatedData = new HashMap<BranchCalKeyHolder, String>();
		fileData.forEach((keyData, dataList) -> {
			Map<BranchCalKeyHolder, String> yearDataDB = branchOperations.getMonthlyData(keyData);
			dataList.forEach((data) -> {
				StringBuffer monthString;
				BranchCalKeyHolder branchCalKeyHolder = new BranchCalKeyHolder(keyData.getYear(),
						keyData.getBranchCode(), data.getMonth());
				if (updatedData.get(branchCalKeyHolder) == null) {
					updatedData.put(branchCalKeyHolder, yearDataDB.get(branchCalKeyHolder));
				}
				monthString = new StringBuffer(updatedData.get(branchCalKeyHolder));
				monthString.setCharAt(data.getDate() - 1, data.getHolidayType());
				updatedData.put(branchCalKeyHolder, monthString.toString());
			});
		});
		return branchOperations.uploadBanchHolidays(updatedData);
	}
}
