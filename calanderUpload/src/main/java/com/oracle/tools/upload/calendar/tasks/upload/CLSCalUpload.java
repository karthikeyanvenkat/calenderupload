package com.oracle.tools.upload.calendar.tasks.upload;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.oracle.tools.upload.calendar.connection.database.IConnectionProvider;
import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.database.operations.CLSCalOperations;
import com.oracle.tools.upload.calendar.dataholders.CLSCalDataHolder;
import com.oracle.tools.upload.calendar.dataholders.CLSCalKeyHolder;
import com.oracle.tools.upload.calendar.parser.CLSFileParser;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Setter;

public class CLSCalUpload extends AbstractTask {
	@Setter
	IConnectionProvider connectionProvider;
	@Setter
	String invalidFileData;
	CLSCalOperations clsCalOperations;

	@Override
	protected boolean process() {
		boolean processResult = true;
		if (ApplicationConstants.getValue("ARGS[3]").equals("CLSCalendar")) {
			Map<CLSCalKeyHolder, List<CLSCalDataHolder>> fileData = null;
			CLSFileParser fileParser = new CLSFileParser();
			Connection connection = null;
			try {
				connection = connectionProvider.getConnection();
			} catch (Exception e) {
				processResult = false;
			}
			if (processResult) {
				clsCalOperations = new CLSCalOperations(connection);
				fileParser.setInvalidFileData(invalidFileData);
				processResult = fileParser.parseFile();
				if (processResult)
					fileData = fileParser.getParsedData();
				if (!fileData.isEmpty()) {
					processResult = clsCalOperations.createYearData(fileData.keySet());
					upload(fileData);
				}
			}
		}

		return processResult;
	}

	private void upload(Map<CLSCalKeyHolder, List<CLSCalDataHolder>> fileData) {
		Map<CLSCalKeyHolder, String> updatedData = new HashMap<CLSCalKeyHolder, String>();
		fileData.forEach((keyData, dataList) -> {
			Map<CLSCalKeyHolder, String> yearDataDB = clsCalOperations.getMonthlyData(keyData);
			dataList.forEach((data) -> {
				StringBuffer monthString;
				CLSCalKeyHolder branchCalKeyHolder = new CLSCalKeyHolder(keyData.getYear(), data.getMonth());
				if (updatedData.get(branchCalKeyHolder) == null) {
					updatedData.put(branchCalKeyHolder, yearDataDB.get(branchCalKeyHolder));
				}
				monthString = new StringBuffer(updatedData.get(branchCalKeyHolder));
				monthString.setCharAt(data.getDate() - 1, data.getHolidayType());
				updatedData.put(branchCalKeyHolder, monthString.toString());
			});
		});
		clsCalOperations.uploadCLSHolidays(updatedData);
	}

}
