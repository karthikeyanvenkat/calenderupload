package com.oracle.tools.upload.calendar.tasks;

import java.util.Map;

import com.oracle.tools.upload.calendar.FileSplitter.IFileSplitter;
import com.oracle.tools.upload.calendar.FileSplitter.NoFileSplit;
import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Getter;
import lombok.Setter;

public class FileSplitResolver extends AbstractTask {

	@Setter
	private Map<String, IFileSplitter> fileSplitterHolder;

	@Setter
	private String sourceRef;

	@Getter
	private IFileSplitter fileSplitter;
	
	@Override
	protected boolean process() {
		if (fileSplitterHolder.containsKey(ApplicationConstants.getValue(sourceRef)))
			fileSplitter = fileSplitterHolder.get(ApplicationConstants.getValue(sourceRef));
		else
			fileSplitter = new NoFileSplit();
		return true;
	}

}
