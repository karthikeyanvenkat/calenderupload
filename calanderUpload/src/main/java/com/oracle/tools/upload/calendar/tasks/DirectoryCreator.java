package com.oracle.tools.upload.calendar.tasks;

import static com.oracle.tools.upload.calendar.logging.LoggingFileType.APPLICATION_LOGGER;
import static com.oracle.tools.upload.calendar.logging.LoggingFileType.ERROR_LOGGER;
import static com.oracle.tools.upload.calendar.logging.LoggingFileType.PERFORMANCE_LOGGER;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.Month;

import com.oracle.tools.upload.calendar.constants.ApplicationConstants;
import com.oracle.tools.upload.calendar.error.ErrorHandler;
import com.oracle.tools.upload.calendar.logging.ApplicationLogger;
import com.oracle.tools.upload.calendar.tasks.enlosers.AbstractTask;

import lombok.Setter;

public class DirectoryCreator extends AbstractTask {
	@Setter
	private String dirCreateFailed, refValue, subDirRefKey;

	@Override
	protected boolean process() {
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "Enter");
		String preProcessFilesPath = ApplicationConstants.getValue(refValue);
		boolean createDirresult = true;
		LocalDateTime currentDateTime = LocalDateTime.now();
		int currentDay = currentDateTime.getDayOfMonth();
		Month currentMonth = currentDateTime.getMonth();
		Integer currentYear = currentDateTime.getYear();
		Integer hour = currentDateTime.getHour();
		Integer minutes = currentDateTime.getMinute();
		Integer seconds = currentDateTime.getSecond();
//		int millis = currentDateTime.get(ChronoField.MILLI_OF_SECOND);
		String subDirName = currentYear + currentMonth.toString().substring(0, 3) + currentDay;
		String subSubDirName = hour.toString() + minutes.toString() + seconds.toString();
		String pathToCreate = preProcessFilesPath + File.separator + ApplicationConstants.getValue("ARGS[3]") + File.separator + subDirName + File.separator + subSubDirName;
		ApplicationLogger.info(APPLICATION_LOGGER, "Creating Directory: " + pathToCreate);
		try {
			Files.createDirectories(Paths.get(pathToCreate));
		} catch (IOException exception) {
			createDirresult = false;
			ErrorHandler.logError(dirCreateFailed);
			ApplicationLogger.fatal(ERROR_LOGGER, "Exception During Directory Creation", exception);
		}
		ApplicationLogger.info(APPLICATION_LOGGER, "Successfully created Directory: " + pathToCreate);
		ApplicationConstants.setValue(subDirRefKey, pathToCreate);
		ApplicationLogger.trace(PERFORMANCE_LOGGER, "Exit");
		return createDirresult;
	}

}
