package com.oracle.tools.upload.calendar.constants;

import java.util.HashMap;
import java.util.Map;

public class ApplicationConstants {
	public static final String WALLET_LOCATION_PROPERTY = "oracle.net.wallet_location";
	public static final String TNS_ADMIN_PROPERTY = "oracle.net.tns_admin";
	public static final String ORACLE_URL_PROPERTY = "jdbc:oracle:thin:/@";
	private static Map<String, String> externalConstants;

	public static void setExternalConstants(Map<String, String> externalConstants) {
		if(externalConstants == null)
			ApplicationConstants.externalConstants = externalConstants;
		else 
			ApplicationConstants.externalConstants.putAll(externalConstants);
	}

	public static String getValue(String key) {
		String returnValue = null;
		if (externalConstants != null) {
			returnValue = externalConstants.get(key);
		}
		return returnValue;
	}
	
	public static void setValue(String key, String value) {
		if(externalConstants == null)
			externalConstants = new HashMap<String, String>();
		externalConstants.put(key, value);
	}
}
